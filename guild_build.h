#ifndef __LIB_GUILD_BUILD_HPP
#define __LIB_GUILD_BUILD_HPP

#include "misc.h"
#include "types.h"
#include "hook_tools.h"
#include <stdio.h>

void HOOK__GUILD_BUILD(void);

#endif
