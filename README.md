### Custom Library for Metin2 Server version 2089

This is a dynamic library for Metin2 Server executable. It is loaded at launch by setting the envariomental
variable LD_PRELOAD (or LD_32_PRELOAD on 64bit system). It fixes a lot of bugs and adds new features. 

It was coded over period of 3 years (2012-2014) on and off, so you will see variable quallity of the code.
I added comments in english to give you the context, but other than that, I didn't change anything.

#### List of features (not all) :
1. Fix for Ghost Hack
2. Fix for Waith Hack
3. Fix for Metin stones not spawning monsters after getting affected with 'slow down'
4. Fix for guild's terrain (doesn't work at all without my fix, unfinished feature ?)
4. New functions for questes
5. New PvP system
6. Team PvP mode.
7. Deathmatch arena
8. Admin's presence is not broadcasted to clients (makes it immposible for cheats to detect the presence of admin)
9. Custom pets system
10. Custom costumes system
11. Custom fishing system (which makes all fishbots obsolete !)
11. A lot of things which were hardcoded are not editable from a config file

##### This code is under GNU v2 license. See LICENSE file for more information.

Patryk Seregiet 2017