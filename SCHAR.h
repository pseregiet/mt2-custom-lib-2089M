#ifndef __TENMA_SCHAR_HPP
#define __TENMA_SCHAR_HPP

/*Reconstruction of original CHARACTER class.
Every player and NPC is a CHARACTER class.
*/

#include <string>
#include <stdarg.h>
#include <cstdio>

#include "types.h"
#include "ITEM.h"
#include "DESC.h"
#include "TEMP_BUFFER.h"
#include "ENTITY.h"
#include "PARTY.h"
#include "GUILD.h"
#include "ITEM_MANAGER.h"
#include "misc.h"

class ITEM;
class DESC;
class PARTY;
class GUILD;
//class ENTITY;

#pragma pack(1)
class SCHAR : public ENTITY
{
public:
	/*Unknown variables are just array of chars....*/
	/*Because this lib and original program was compiled with GCC/G++
	all variables of child classes are at the beggining, unlike VS
	at the end. Because of that at the beggining i have char unkn4[44] with comment 60 - 111..
	first 60 bytes are ENTITY class (also HorseRider class, but i don't use it so i didn't re-create it in my code)
	*/
	char unkn4[44]; // 60 - 111
	char horse_grade; // 112
	char unkn9[3]; // 113 - 115
	char horse_level; // 116
	char horse_is_riding; // 117
	short horse_stamina; // 118 - 119
	short horse_life; // 120 - 121
	char unkn6[114]; // 122 - 235
	int poly_vnum; // 236 - 239
	char unkn10[8]; // 240 - 247
	int player_id; // 248 - 251
	int vid; // 252 - 255
	char unkn8[8]; // 256 - 263
	char char_type; // 264
	long points[255];
	char unkn42251[3];
	char job; // 1288
	char voice; // 1289
	unsigned char level; // 1290
	char unknshuet;
	int exp; // 1292 - 1295
	int gold; // 1296 - 1299
	int hp; // 1300 - 1303
	int sp; // 1304 - 1307
	char unkn19[1040]; // 1308 - 2347
	int max_hp; // 2348 - 2351
	int max_sp; // 2352 - 2355
	char unkn16[644]; // 2356 - 2999
	char gm_level; // 3000
	char bBasePart;
	int iMaxStamina;
	BYTE bBlockMode;
	char unkn21[80]; // 3003 - 3087
	int dmap_index; // 3088 - 3091
	char unkn17[133]; // 3092 - 3223
	PARTY* party; //3224 - 3227 struktura prawdopodobnie;
	char unkn18[32]; // 3228 - 3259
	int DungeonStructure; // 3260 - 3263
	int unkn97;
	GUILD* guild; // 3268 - 3271 struktura !
	char unkn20[372];// 3272 - 3667
	SCHAR* horse;
	SCHAR* rider; //3648 - 3651
	int mount_vnum; // 3652 - 3655
	int mount_time; // 3656 - 3659
	char unkm24[5];
	char empire; //3665
	char unkn22[30]; // 3666 - 3695
	ITEM* quest_item_ptr; //3696-3699
	char unkn23[12]; // 3670 - 3795
	Event* DeadEvent; // 3712 - 3715
	Event* m_pkStunEvent;
	Event* m_pkSaveEvent;
	Event* m_pkRecoveryEvent;
	Event* m_pkTimedEvent;
	Event* m_pkFishingEvent;
	Event* m_pkAffectEvent;
	Event* m_pkPoisonEvent;
	Event* m_pkFireEvent;
	Event* m_WarpNPCEvent;
	Event* m_pkMiningEvent;
	Event* m_pkWarpEvent;
	Event* m_pkCheckSpeedHackEvent;
	Event* m_pkDestroyWhenIdleEvent;
	Event* m_pkSetSystemUpdateEvent;
	bool m_bHasPoisoned;
	char unkn26[23];
	int mob_name_p; //3796
	int godSpeed; // 3800
	char unkn25[187]; // 3801 - 3988

	/*Variables below are my own new additions. Original structure is 3988 bytes in size */
	char isDestroyed;
	char pet_state;
	char isGuard;
	int lastCheckStrengthTime;
	char hungryPercent;
	Event* costume_main_event;
	Event* costume_hair_event;
	Event* NewDeadEvent;
	Event* meter_event;
	int pvp_opponent_vid;
	char can_trade;
	Event* fishing_event;
	BYTE wh_log_count;
	Event* fishing[4];
	char bShowStat;
	int wh_time;
	SCHAR* pet;
	char b_petexp;
	unsigned int lobby;
	unsigned int pvp_agree_time;
	unsigned int pvp_time;
	char arena_kill_count;
	char fishing_set_count;
	char fisging_cur_cound;
	char fishing_catch;

	char* GetName(void);
	int CreateFly(char a1, SCHAR* a2);
	void ChatPacket(char a1, const char* a2, ...);
	int GetRaceNum(void);
	int SetRider(SCHAR* a1);
	int GetQuestFlag(std::string a1);
	int Show(int a1, int a2, int a3, int a4, int a5);
	int Stun(void);
	int AddAffect(int a1, char a2, int a3, int a4, int a5, int a6, char a7, char a8);
	int SetWarp(int a1, int a2, int a3);
	int SetQuestFlag(std::string a1, int a2);
	int ChangePoint(char a1, int a2, char a3, char a4);
	int BeginFight(SCHAR *  a1);
	int CountSpecifyItem(int a1);
	void RemoveSpecifyItem(int a1, int a2);
	int RemoveAffect(int a1);
	int GetPoint(unsigned char a1);
	void SetPoint(unsigned char a1, int a2);
	void SetPKMode(char a1);
	void SetCoward(void);
	bool IsDead(void);
	int CountItem(int a1);
	void UnEquipRideItem(void);
	char RequestParty(SCHAR* a1);
	char RequestPartyAccept(SCHAR* a1);
	char PartyJoin(SCHAR* a1);
	char PartyInvite(SCHAR* a1);
	char GetPKMode(void);
	void GoTo(int a1, int a2);
	ITEM* GetWear(char a1);
	ITEM* GetItem(int a1);
	ITEM* AutoGiveItem(int a1, unsigned char a2, int a3, char a4);
	int FindAffect(int a1, char a2);
	int SetPart(char a1, short a2);
	int UpdatePacket(void);
	int GetEmptyInventory(int a1);
	void ComputePoints(void);
	void PointsPacket(void);
	short GetPart(char a1);
	void MoveItem(char a1, char a2, unsigned char a3);
	int SwapItem(char a1, char a2);
	int GetJob(void);
	bool IsRiding(void);
	int GerRealPoint(BYTE a1);
	void SetSkillLevel(int a1, int a2);
	void SkillLevelPacket(void);
	int FishTake(void);
	int Fishing(void);
	int GetSkillLevel(int a1);

	void ActEmotion(const char* emotion);


	void SaveExitLocation(void);
	void CreateFlyOnlyForMe(char type, SCHAR* target);
	bool IsStun(void);
	void ChatToEveryone(const char* msg, char type);
	void StartRecoveryEvent(void);
	void SetPosition(int a1);
	void ExitToSavedLocation(void);
	void ReviveInvisible(int a1);

	void RefreshCostumeBonuses(void);

	//int PartyLiveCount(void);
};

#pragma pack()


#endif
