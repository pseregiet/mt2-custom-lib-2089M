#ifndef __LIB_ITEM_MANAGER_HPP
#define __LIB_ITEM_MANAGER_HPP

#include "types.h"
#include "item_proto.h"
#include "ITEM.h"
#include "GameSingleton.h"

#define ITEM_MANAGER_SINGLETON (ITEM_MANAGER*)*(DWORD*)(0x84C5B18)

#pragma once
class ITEM_MANAGER : public GameSingleton <ITEM_MANAGER, 0x84C5B18>
{
public:
	void RemoveItem(ITEM* a1, char* name_log);
	ITEM* CreateItem(int a1, int a2, int a3, char a4, int a5);
	void DestroyItem(ITEM* a1);
	item_proto* GetTable(int id);
};

#endif
