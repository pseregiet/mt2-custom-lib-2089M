#ifndef __LIB_DUNGEON_HPP
#define __LIB_DUNGEON_HPP

#include "PARTY.h"
#include "SCHAR.h"

class SCHAR;
class PARTY;
#pragma pack(1)
class DUNGEON
{
private:
	DWORD m_lOrigMapIndex;
	DWORD m_lMapIndex;
public:
	void Join(SCHAR* pc);
	SCHAR* SpawnMob(int race, int x, int y);
	void Notice(const char* msg);
	void JoinParty(PARTY* party);
	int GetMapIndex(void);
	void JoinOnPosition(SCHAR* pc, int x, int y);
	void JoinPartyOnPosition(PARTY* party, int x, int y);
	void SetFlag(std::string flag, int val);
};
#pragma pack()

#endif


