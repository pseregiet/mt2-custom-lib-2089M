#ifndef __LIB_HOOK_TOOLS
#define __LIB_HOOK_TOOLS

#include <sys/mman.h>
#include <string.h>

extern "C" void JmpPatch(int pDest, int pSrc, int nNops);
extern "C" void CallPatch(int pDest, int pSrc, int nNops);
extern "C" void patch(int x, unsigned char* y, int z);
extern "C" void fill_nop(int x, int z);

#endif
