#include "DUNGEON.h"


//DUNGEON::DUNGEON(void)
//{
//}


//DUNGEON::~DUNGEON(void)
//{
//}


void DUNGEON::Join(SCHAR* pc)
{
	((void(*)(DUNGEON*, SCHAR*))0x080F6000)(this,pc);
}


SCHAR* DUNGEON::SpawnMob(int race, int x, int y)
{
	return ((SCHAR*(*)(DUNGEON*,int,int,int))0x080F7600)(this,race,x,y);
}


void DUNGEON::Notice(const char* msg)
{
	((void(*)(DUNGEON*, const char*))0x080F63A0)(this,msg);
}


void DUNGEON::JoinParty(PARTY* party)
{
	((void(*)(DUNGEON*,PARTY*))0x080F92A0)(this,party);
}

int DUNGEON::GetMapIndex(void)
{
	return this->m_lMapIndex;
}


void DUNGEON::JoinOnPosition(SCHAR* pc, int x, int y)
{
	pc->SaveExitLocation();
	pc->SetWarp(x, y, this->m_lMapIndex);
}


void DUNGEON::JoinPartyOnPosition(PARTY* party, int x, int y)
{
	party->SetDungeon(this);
	char xx[8];
	*(int*)(xx) = (int)party;
	*(int*)(xx+4) = 0;
	((void(*)(DUNGEON*,char*))0x80F9D00)(this + 56, xx);

	char yy[12];
	*(int*)(yy) = this->m_lOrigMapIndex;
	*(int*)(yy+4) = x;
	*(int*)(yy + 8) = y;

	((void(*)(PARTY*, char*))0x80F9EA0)(party, yy); // ForEachPartyMember<DungeonWarp>
}


void DUNGEON::SetFlag(std::string flag, int val)
{
	((void(*)(DUNGEON*, std::string, int))0x80F5A50)(this,flag,val);
}

