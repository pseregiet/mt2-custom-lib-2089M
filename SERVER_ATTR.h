#ifndef __LIB_SERVER_ATTR_HPP
#define __LIB_SERVER_ATTR_HPP

#include <string>
#include <vector>
#include <stdio.h>
#include "misc.h"

/*The attribute file is a file which defines where player can move on the map, where's water, where's 
safezone and such. Client-Side it's very simple thing. It's just a raw file 256x256 (maps are build of quadrants 256x256 each)
and each byte represents some attribute. On the Server-Side, for some reason, they use one file for the whole map
and I couldn't figure out how this file was structured. That's a big problem, pretty much all custom maps people made
had a clear server-attr, sometimes the map creator would make his own server-attr, but never explained how it works.
So I decided to hook the functions that read the Server-Attr and make them work with the Client-Side files. SO EASY !
*/


#pragma once
class SERVER_ATTR
{
private:
	std::vector< char* > attrs;
	int x_size;
	int y_size;

public:
	SERVER_ATTR(void);
	~SERVER_ATTR(void);

	unsigned char GetAttr(int x, int y);
	bool Initialize(const char* map_name, int x, int y);
};

#endif



