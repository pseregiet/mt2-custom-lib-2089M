#include "small_hooks.h"

extern "C" void SCHAR__ChangePoint_sura_skill_fix(SCHAR* a1, char type, int amount, bool bAmount, bool bBroadcast)
{
	if (amount < 0)
	{
		return;
	}
	a1->ChangePoint(type, amount, bAmount, bBroadcast);
}

extern "C" void SCHAR__Initialize(SCHAR* a1)
{
	( ( void ( * ) ( SCHAR* ) ) 0x8074DB0 )(a1); // SCHAR::Initialize
	memset((void*)((int)a1 + 3988), 0, 79);
	/*basically a CHARACTER's constructor. Im just memseting my new variables to zero*/
}

void HOOK_SCHAR(int offset)
{
	//Konstruktor SCHAR
	CallPatch((int)&SCHAR__Initialize, IPS(offset+0x0807DA5D), 0);
	CallPatch((int)&SCHAR__Initialize, IPS(offset+0x0807E00D), 0);
	// rozszerzenie struktury gracza. Standardowy rozmiar 3988
	mprotect((const void*)IPS(offset+0x80A6E00), 4u, 7);
	*(unsigned short*)(IPS(offset+0x80A6E00)) = 4067; // nie wychodzic poza 4095 bo umrzemy /* don't go above 4095 or else we'll die*/

	/*The reason for 4095 number is this :

	4095 in hex is 0xFFF, and every new CHARACTER's object will start on address that ends with 3 zeros, for example 0x12345000
	We can go for more than 4095 if we want, but it will just take a bit more memory, because it will always try to create object on a even address.
	and the memory between object will be not used

	*/

	//printf("HOOk_SCHAR OK\n");
}

void HOOK_EXP_TABLE(void)
{
	int exp_table[120] = {
						0,
						150,
						400,
						750,
						1250,
						2150,
						3100,
						5500,
						8500,
						12000,
						16500,
						21500,
						24000,
						38000,
						50000,
						65000,
						84500,
						109500,
						141500,
						182500,
						236000,
						305000,
						352500,
						406500,
						468500,
						538500,
						618500,
						709000,
						812000,
						928500,
						1061000,
						1210500,
						1380500,
						1572500,
						1790000,
						2036500,
						2316000,
						2597000,
						2858500,
						3132000,
						3418500,
						3800000,
						4137000,
						4495000,
						4876500,
						5280000,
						5705000,
						6160000,
						6635000,
						7140000,
						7670000,
						8435000,
						9480000,
						9990000,
						10710000,
						11465000,
						12290000,
						13100000,
						13980000,
						14900000,
						16390000,
						18030000,
						19835000,
						21820000,
						24000000,
						26400000,
						29040000,
						31945000,
						35140000,
						38655000,
						42520000,
						46770000,
						51450000,
						56750000,
						60500000,
						68500000,
						75350000,
						82850000,
						118495000,
						130325000,
						143390000,
						157500000,
						173485000,
						190840000,
						209885000,
						230880000,
						254020000,
						279370000,
						307320000,
						338065000,
						371865000,
						520611000,
						572672100,
						629939310,
						692933241,
						762226565,
						838449222,
						922294144,
						1014523558,
						1025000000,
						1075000000,
						1105000000,
						1125000000,
						1140000000,
						1155000000,
						1230000000,
						1270000000,
						1350000000,
						1420000000,
						1480000000,
						1530000000,
						1590000000,
						1640000000,
						1690000000,
						1755000000,
						1800000000,
						1860000000,
						1940000000,
						2050000000,
						2147483646
};
	mprotect((const void*)EXP_TABLE_COMMON, 120*4, 7);
	for (int i = 0;i<120;i++)
	{
		*(DWORD*)(EXP_TABLE_COMMON+4*i) = exp_table[i];
	}

	//printf("EXP_TABLE OK\n");
}


//0x81AEE70 (qm, vid, race)

int WhenDead_c(SCHAR* a1)
{
	bool isd = a1->IsDead();
	if (!isd)
	{
		if (a1->lpDesc) // if a CHARACTER has lpDesc, it's a player, otherwise, NPC
		{
			((void(*)(int,DWORD,DWORD))0x81AEE70)(*(DWORD*)0x84C4860, a1->player_id, 9999);
			/* When you write your quest (in LUA) you have few triggers such as when kill, when login, when click
			   etc. It's quite hard to add another one, like this one, that triggers when the player dies, so instead
			   I use the trigger for kill with the monster ID 9999. This monster doesn't exist, so it's not a problem
			   And I can create as many triggers as I desire with this method. In the quest i just write
			   "when 9999.kill begin" which works like "when player.dead begin" would */
		}
	}
	return isd;
}

/*Ghost hack is a hack that allows you to do some things while beign dead
Fix is easy, just check if player is dead*/

extern "C" DWORD GhostHack_c(SCHAR* a1)
{
	if (a1->IsDead() ||
		! ((bool(*)(SCHAR*))0x806D620)(a1) )
	{
		return 0x812B75B;
	}
	return 0x0812B724;
}


extern "C" void PcKillsPC_c(SCHAR* victim, SCHAR* killer)
{
	if (victim->bShowStat)
	{
		victim->ChatPacket(1, "HP Przeciwnika (%d/%d)", killer->hp, killer->max_hp); // HP Przeciwnika (%d/%d)
	}
	if (killer->map_index == 255)
	{
		std::string qf1 = "arena_dm.points"; //arena_dm.points

		int vlevel = victim->level;
		int mlevel = killer->level;

		int points = 0;
		int add_pkt = 0;
		int add_pkt2 = 0;

		points = 10 + vlevel - mlevel;

		if (points > 0)
		{
			killer->arena_kill_count++;
			killer->ChatPacket(1, locale_find("get%dpt"), points); // Otrzyma�e� %d punkt�w -- get%dpt
		}
		/*kill streaks are fun !*/
		if (killer->arena_kill_count < 31)
		{
			char* name = killer->GetName();
			char masg[50];
			if (killer->arena_kill_count == 30)
			{
				sprintf(masg, locale_find("smnkh"), name);// Niech Kto� Zabije %s !! -- smnkh
				SendNoticeMap(masg, 255, 0);
				killer->ChatToEveryone(masg, 5);
			}
			else if (killer->arena_kill_count == 25)
			{
				sprintf(masg, locale_find("smn%sgod"), name); // %s jest Nczym B�g ! -- smn%sgod
				SendNoticeMap(masg, 255, 0);
				killer->ChatToEveryone(masg, 5);
			}
			else if (killer->arena_kill_count == 20)
			{
				sprintf(masg, "%s jest Nie do Zatrzymania !", name); // %s jest Nie do Zatrzymania !
				SendNoticeMap(masg, 255, 0);
				killer->ChatToEveryone(masg, 5);
			}
			else if (killer->arena_kill_count == 15)
			{
				sprintf(masg, "%s Dominuje !", name); //%s Dominuje !
				SendNoticeMap(masg, 255, 0);
				killer->ChatToEveryone(masg, 5);
			}
			else if (killer->arena_kill_count == 10)
			{
				sprintf(masg, "%s Jest W Szale Zabijania !", name);//%s Jest W Szale Zabijania !
				SendNoticeMap(masg, 255, 0);
				killer->ChatToEveryone(masg, 5);
			}
			else if (killer->arena_kill_count == 5)
			{
				sprintf(masg, locale_find("%skseri"), name); // %s Dokonuje Serii Zab�jstw ! -- %skseri
				SendNoticeMap(masg, 255, 0);
				killer->ChatToEveryone(masg, 5);
			}
			if (killer->arena_kill_count % 5 == 0)
			{
				add_pkt = killer->arena_kill_count;
				killer->ChatPacket(1, locale_find("add@dpk"), add_pkt); //Dostajesz dodatkowe %d punkt�w -- add@dpk
			}
			if (victim->arena_kill_count > 4)
			{
				add_pkt2 = victim->arena_kill_count/5;
				add_pkt2 *= 5;
				sprintf(masg, locale_find("prze222fra"), name, victim->GetName()); //%s Przerywa sesje zab�jstw gracza %s -- prze222fra
				SendNoticeMap(masg, 255, 0);
				killer->ChatPacket(1, locale_find("woahpkts"), add_pkt2); //Dostajesz dodatkowe %d punkt�w -- woahpkts
			}
			points += add_pkt + add_pkt2;
			killer->SetQuestFlag(qf1, points+killer->GetQuestFlag(qf1));
			victim->arena_kill_count = 0;
		}
	}
}
extern "C" void PcKillsPC_asm(void)
{
	asm(".intel_syntax noprefix\n"
		".globl PcKillsPC_c\n"
		"add esp, 4\n"
		"mov eax, [ebp - 0x1A0]\n"
		"push eax\n"
		"push edi\n"
		"call PcKillsPC_c\n"
		"push 0x0808937D\n"
		"ret\n"
		);
}

/*Wait hack is a hack where you can attack many targets on large distances very fast while... stading
This bug exist because this game depends to much on client, so you can replace waiting animation
with attacking, change few things and by that you have this abbomination... I couldn't find a real
cure for it withough RADICAL changes, so I did as best as i could.

This checks how much time passed since you send an attack packet while you hit something.
Because waithack attacks targets without sending actuall "ATTACK" packet (for broadcasting animation)
I can determinaty by it, that someone is using WaitHack(simply called WH) I added a relativelly speaking big time window
because of large amount of false positives when on weak interent connection. Also, this system, even though it works 99% times,
it still triggers false alarm sometimes. That's why i changed kicking player from a server to a log that i can read,
go to a game, check the player for myself and ban him or leave him. This decision has other adventage as well

Player doesn't know when my system detects the hack, if he would get kick, he could discover the patern
and bypass it (and it's quite easy if you know how this system works). I would do it properly with source code... 
oh well, it works pretty good anyway !
*/
extern "C" void WHCheck_c(DESC* desc, BYTE a2, BYTE a3)
{
	SCHAR* a1 = desc->m_lpSCHAR;

	DWORD x = a1->wh_time;
	if (x < get_global_time())
	{
		int count = *(DWORD*)((DWORD)a1 + 3940);
		count++;
		if (count > 8)
		{
			//log do wh
			if (a1->gm_level)
			{
				a1->ChatPacket(3, "WaitHack detected !"); // WaitHack detected !
			}
			*(DWORD*)((DWORD)a1 + 3940) = 0;
			a1->wh_log_count++;
			if (a1->wh_log_count >= 5)
			{
				char inf[300];
				sprintf(inf, "WH : Ch%d, %s (%d)", CHANNEL, a1->GetName(), a1->map_index); // WH : Ch%d, %s (%d)
				//gg tutaj
				/* I had whole system of sending this log to my gg (polish instant communicator like AOL or AIM) but it got lost.... */
				a1->wh_log_count = 0;
			}
		}
	}
	((void(*)(DESC*,BYTE,BYTE))0x80EA440)(desc, a2, a3);
}

extern "C" DWORD WHSetTime_c(SCHAR* a1, char a2)
{
	if(a1->lpDesc)
	{
		if (a2 != 1)
		{
			DWORD x = get_global_time()+1;
			a1->wh_time = x;
		}
	}
	return GhostHack_c(a1);
}

extern "C" void WHSetTime_asm(void)
{
	asm(".intel_syntax noprefix\n"
		".globl WHSetTime_c\n"
		"add esp, 4\n"
		"movzx eax, byte ptr [edi+1]\n"
		"push eax\n"
		"push esi\n"
		"call WHSetTime_c\n"
		"add esp, 8\n"
		"jmp eax\n"
		);
}

extern "C" void DevilTowerQF_hook(SCHAR* a1, std::string a2, int a3) // 0x0812CCA4
{
	int get = a1->GetQuestFlag(a2);
	a1->SetQuestFlag(a2, get-1);
}
extern "C" bool DevilTowerITEM_hook(SCHAR* a1, ITEM* a2, char a3)
{
	if (a1->map_index >= 660000 && a1->map_index <= 66999)
	{
		if (a2->ItemProto->item_limit[0].LimitValue > 90)
		{
			a1->ChatPacket(1, locale_find("candt90")); //Nie mo�esz ulepszy� tego przedmiotu (udaj si� do innej wie�y) -- candt90
			return 0;
		}
		((void(*)(SCHAR*,ITEM*,bool))0x8096F10)(a1,a2,a3); // CHARACTER::DoRefine
		return 1;
	}
	return 0;
}
extern "C" void DevilTowerITEM_hook_asm()
{
	asm(".intel_syntax noprefix\n"
		".globl DevilTowerITEM_hook\n"
		"add esp, 4\n"
		"call DevilTowerITEM_hook\n"
		"test eax, eax\n"
		"jz no_upgrade_dt\n"

		"push 0x0812CC7E\n"
		"ret\n"

		"no_upgrade_dt:\n"

		"push 0x812CAE1\n"
		"ret\n"
	);
}
extern "C" int FireGhostDontAttackGM(SCHAR* a1, SCHAR* a2)
{
	if (a2->gm_level || *(BYTE*)((DWORD)a2+4)) // a2->isObserver
	{
		return 0;
	}
	return ((int(*)(SCHAR*, SCHAR*))0x8060CC0)(a1, a2);
}
extern "C" char FireGhostHorseFix(int a2, int a1)
{
	int x = ((int(*)(int))0x08116650)(a1); // CHorseRider::GetHorseGrade
	if (x<3)
	{
		return 1;
	}
	if (a2 != 78)
	{
		return 1;
	}
	return 0;
}
extern "C" void FireGhostHorseFix_asm(void)
{
	asm(".intel_syntax noprefix\n"
		".globl FireGhostHorseFix\n"
		"add esp, 4\n"
		"push eax\n"
		"mov eax, [ebp+12]\n"
		"push eax\n"
		"call FireGhostHorseFix\n"
		"add esp, 8\n"
		"mov esi, eax\n"
		"test eax, eax\n"
		"jnz fail_duszek\n"
		"mov eax, 0x80B374A\n"
		"jmp eax\n"

		"fail_duszek:\n"

		"mov eax, 0x80B43F0\n"
		"jmp eax\n"
	);
}

/*In this game you have 3 types of horses and each type uses it's own food
but you can feed your horse with bad kind of food by draging it on someone's 
else horse that is the same kind as the food you have. It will sill feed your horse.
This fixes it*/
extern "C" int HorseFeedFix_c(SCHAR* kon, SCHAR* gracz, ITEM* a3)
{
	SCHAR* rider = kon->rider;
	if (rider && rider != gracz)
	{
		gracz->ChatPacket(1, "To nie jest tw�j ko�");
		return 0x80941A0;
	}
	int id = kon->GetRaceNum();
	int idi = a3->ItemProto->ID;
	if (id >= 20101 && id <= 20103)
	{
		if (idi != 50054)
		{
			gracz->ChatPacket(1, "Tw�j ko� nie moze tego zjec� ");
			return 0x80941A0;
		}
		return 0x8094346;
	}
	else if (id >= 20104 && id <= 20106)
	{
		if (idi != 50055)
		{
			gracz->ChatPacket(1, "Tw�j ko� nie moze tego zjec� ");
			return 0x80941A0;
		}
		return 0x8094346;
	}
	else if (id >= 20107 && id <= 20109)
	{
		if (idi != 50056)
		{
			gracz->ChatPacket(1, "Tw�j ko� nie moze tego zjec� ");
			return 0x80941A0;
		}
		return 0x8094346;
	}
	return 0x80941A0;
}
extern "C" void HorseFeedFix_asm(void)
{
	asm(".intel_syntax noprefix\n"
		".globl HorseFeedFix_c\n"
		"add esp, 4\n"
		"mov eax, [ebp+16]\n"
		"push eax\n"
		"mov eax, [ebp+12]\n"
		"push eax\n"
		"mov eax, [ebp+8]\n"
		"push eax\n"
		"call HorseFeedFix_c\n"
		"add esp, 12\n"
		"jmp eax\n"
	);
}

void HOOK__SMALL_HOOKS(int offset)
{
	HOOK_SCHAR(offset);
	CallPatch((int)&SCHAR__ChangePoint_sura_skill_fix,		IPS(offset+0x0808B318), 0);

	CallPatch((int)&WhenDead_c,								IPS(offset+0x08088930), 0);

	CallPatch((int)&PcKillsPC_asm+3,						IPS(offset+0x08089353), 0);

	CallPatch((int)&WHCheck_c,								IPS(offset+0x0812E82A), 0);
	CallPatch((int)&WHSetTime_asm+3,						IPS(offset+0x0812B71F), 0);

	CallPatch((int)&DevilTowerQF_hook,						IPS(offset+ 0x0812CCA4), 0);
	CallPatch((int)&DevilTowerITEM_hook_asm+3,				IPS(offset+ 0x0812CC79), 0);

	CallPatch((int)&FireGhostDontAttackGM,					IPS(offset + 0x080B760D), 0);
	CallPatch((int)&FireGhostHorseFix_asm+3,				IPS(offset + 0x080B441B), 0);

	CallPatch((int)&HorseFeedFix_asm+3,						IPS(offset + 0x08094190), 0);

	HOOK_EXP_TABLE();
}
