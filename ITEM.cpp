#include "ITEM.h"

int ITEM::SetAttr(int a1, char a2, short a3)
{
	return ((int(*)(ITEM*,int, char, short))(0x0813B780 + 0x3A0))(this,a1, a2, a3);
}
int ITEM::UpdatePacket(void)
{
	return ((int(*)(ITEM*))(0x8137AF0 + 0x3A0))(this);
}
int ITEM::Save(void)
{
	return ((int(*)(ITEM*))(0x81391A0 + 0x3A0))(this);
}
int ITEM::AddAttr(void)
{
	return ((int(*)(ITEM*))(0x0813C720 + 0x3A0))(this);
}
int ITEM::GetAttrCount(void)
{
	return ((int(*)(ITEM*))(0x813B5D0 + 0x3A0))(this);
}
int ITEM::SetSocket(int a1, int a2)
{
	return ((int(*)(ITEM*,int, int))(0x8139220 + 0x3A0))(this,a1, a2);
}
int ITEM::CanPutInto(ITEM* a1)
{
	return ((int(*)(ITEM*,ITEM*))(0x8138210 + 0x3A0))(this,a1);
}
int ITEM::IsAccesoryForSocket(void)
{
	return ((int(*)(ITEM*))(0x8137660 + 0x3A0))(this);
}
void ITEM::SetOwnership(SCHAR* a1, int a2)
{
	return ((void(*)(ITEM*,SCHAR*, int))(0x8139FF0 + 0x3A0))(this,a1, a2);
}
void ITEM::StartDestroyEvent(int a1)
{
	return ((void(*)(ITEM*,int))(0x813A1F0 + 0x3A0))(this,a1);
}
void ITEM::AddToGround(int a1, int a2)
{
	return ((void(*)(ITEM*,int, int))(0x81398A0 + 0x3A0))(this,a1, a2);
}
int ITEM::AutoGiveItem(int a1, int a2, int a3, bool a4)
{
	return ((int(*)(ITEM*,int, int, int, bool))(0x08095270 + 0x3A0))(this,a1, a2, a3, a4);
}
int ITEM::GetSpecialGroup(void)
{
	return ((int(*)(ITEM*))(0x08138D90))(this);
}
int ITEM::GetCount(void)
{
	return ((int(*)(ITEM*))(0x8137A20 + 0x3A0))(this);
}
void ITEM::SetCount(int a1)
{
	return ((void(*)(ITEM*,int))(0x813A880 + 0x3A0))(this,a1);
}
void ITEM::AddToSCHAR(SCHAR* a1, int a2)
{
	return ((void(*)(ITEM*,SCHAR*, int))(0x08139A30 + 0x3A0))(this,a1, a2);
}
void ITEM::EquipTo(SCHAR* a1, char a2)
{
	return ((void(*)(ITEM*,SCHAR*, char))(0x0813AB50 + 0x3A0))(this,a1, a2);
}
void ITEM::StartUniqueExpireEvent(void)
{
	return ((void(*)(ITEM*))(0x08139F00 + 0x3A0))(this);
}
void ITEM::ChangeAttribute(int a1)
{
	return ((void(*)(ITEM*,int))(0x813C770 + 0x3A0))(this,a1);
}

