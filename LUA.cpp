#include "LUA.h"


int LUA::pushnumber(double a1)
{
	return ((int(*)(LUA*,double))(0x0831C9C0 + 0x6D0))(this,a1);
}
long double LUA::tonumber(int a1)
{
	return ((long double(*)(LUA*,int))(0x831D770 + 0x6D0))(this,a1);
}
char* LUA::tostring(int a1)
{
	return ((char*(*)(LUA*,int))(0x0831D700 + 0x6D0))(this,a1);
}
int LUA::pushstring(const char* a1)
{
	return ((int(*)(LUA*,const char*))(0x0831D660 + 0x6D0))(this,a1);
}
char LUA::isNumber(int a1)
{
	return ((char(*)(LUA*,int))(0x831D7B0 + 0x6D0))(this,a1);
}
void LUA::saqf(void)
{
	return ((void(*)(LUA*))(0x081A2930 + 0x6D0))(this);
}
int LUA::gaqf(void)
{
	return ((int(*)(LUA*))(0x081A2560 + 0x6D0))(this);
}
void LUA::pushbool(bool a1)
{
	return ((void(*)(LUA*,bool))(0x831C9E0 + 0x6D0))(this,a1);
}
void LUA::newtable(void)
{
	return ((void(*)(LUA*))(0x0831d480 + 0x6D0))(this);
}
void LUA::rawset(int a1)
{
	return ((void(*)(LUA*,int))(0x0831d3d0 + 0x6D0))(this,a1);
}

