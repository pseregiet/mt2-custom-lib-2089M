#include "SERVER_ATTR_MANAGER.h"


SERVER_ATTR_MANAGER::SERVER_ATTR_MANAGER(void)
{
}


SERVER_ATTR_MANAGER::~SERVER_ATTR_MANAGER(void)
{
	for (unsigned int i = 0; i < this->server_attr_maps.size(); i++)
	{
		delete this->server_attr_maps[i];
	}
	this->server_attr_maps.clear();
	this->maps.clear();
}

void SERVER_ATTR_MANAGER::Initialize(void)
{
	FILE* f;
	f = fopen("locale/germany/server_attr.txt", "r");
	if (f == NULL)
	{
		printf("Blad przy czytaniu server_attr.txt\n");
		return;
	}
	char line[100];
	char* pch;
	while (!feof(f))
	{
		fgets(line, 100, f);
		pch = strtok (line,"	");
		int co = 0;
		sa_map s;
		while (pch != NULL)
		{
			if (!co)
			{
				strcpy(s.name, pch);
			}
			else
			{
				int x = (int)&s;
				*(int*)(x+4*co) = atoi(pch);
			}
			pch = strtok (NULL, "	");
			co++;
		}
		SERVER_ATTR* attr = new SERVER_ATTR();
		if (attr->Initialize(s.name, s.x_size, s.y_size))
		{
			this->maps.push_back(s.index);
			this->server_attr_maps.push_back(attr);
		}
		else
		{
			delete attr;
			((void(*)(const char*,int,const char*,...))sys_err)("SERVER_ATTR_MANAGER", 13, "attr->Initialize(%s, %d, %d) fail !", s.name, s.x_size, s.y_size);
		}
	}
	fclose(f);
}
