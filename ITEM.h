#ifndef __LIB_ITEM_HPP
#define __LIB_ITEM_HPP

#include "types.h"
#include "item_proto.h"
#include "ENTITY.h"

class SCHAR;
/*ITEM class is a class of one UNIQUE item in game. item_proto is class/structure of "templates" of items. 
For example : Static info about item, like who can use it, what does it do etc. is in item_proto,
non constant variables, like owner of the item, random bonuses etc are in this class */
#pragma pack(1)
class ITEM : public ENTITY
{
public:
	item_proto* ItemProto; // 68 - 71
	char unkn4[23]; // 72 - 95
	int Count; // 96-99
	int flags; // 100 - 103 - jestem na 99% ze to jest antiflag
	char unkn2[9]; // 104 - 111
	int socket[3];
	ITEM_BONUS bonus[5];
	Event* UniqueExpireEvent; // 152 - 154
	char unkn5[9];
	int OwnerVID; // 164

	int SetAttr(int a1, char a2, short a3);
	int UpdatePacket(void);
	int Save(void);
	int AddAttr(void);
	int GetAttrCount(void);
	int SetSocket(int a1, int a2);
	int CanPutInto(ITEM* a1);
	int IsAccesoryForSocket(void);
	void SetOwnership(SCHAR* a1, int a2);
	void StartDestroyEvent(int a1);
	void AddToGround(int a1, int a2);
	int AutoGiveItem(int a1, int a2, int a3, bool a4);
	int GetSpecialGroup(void);
	int GetCount(void);
	void SetCount(int a1);
	void AddToSCHAR(SCHAR* a1, int a2);
	void EquipTo(SCHAR* a1, char a2);
	void StartUniqueExpireEvent(void);
	void ChangeAttribute(int a1);
};
#pragma pack()


#endif
