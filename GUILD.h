#ifndef __LIB_GUILD_HPP
#define __LIB_GUILD_HPP

#include "types.h"
/*Original GUILD class is much bigger (184 bytes iirc) but these 4 variables 
are first, so i didn't have to use any 'unknown char arrays' */
#pragma pack(1)
class GUILD
{
public:
	DWORD guild_id;
	DWORD master_pid;
	DWORD exp;
	BYTE level;

	bool UnderWar(DWORD guild_id);
};
#pragma pack()

#endif


