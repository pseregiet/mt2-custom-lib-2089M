#include "chat.h"

const char* empires[3] = {"Schinsoo", "Chunjo", "Jinno"};

extern "C" int WhisperLink_c(char* name, char* msg, char* where, char* type, SCHAR* a1)
{
	int ret = 0;
	if (*(type+3) == 6)
	{
		if (msg[0] == 0x21)
		{
			ret = sprintf(where, "!|cfff1e6c0|Htenma:2:%s:1:1:1|h[(%s) %s]|h|r : %s",name, empires[a1->empire], name, msg+1);
		}
		else
		{
			ret = sprintf(where, "|cfff1e6c0|Htenma:2:%s:1:1:1|h[%s]|h|r : %s", name, name, msg);
		}
	}
	else
	{
		ret = sprintf(where, "%s : %s", name, msg);
	}

	return ret;
}
extern "C" void WhisperLink_asm(void)
{
	asm(".intel_syntax noprefix\n"
		".globl WhisperLink_c\n"
		"add esp, 4\n"
		"push edi\n"
		"push ebx\n"
		"push ecx\n"
		"push edx\n"
		"push eax\n"
		"call WhisperLink_c\n"
		"add esp, 20\n"
		"push 0x0812FEC5\n"
		"ret\n"
		);
}

char* FuncShout(int a1, int a2, char* a3, char a4)
{
	SCHAR* p = 0;
	for (int i = a1; i != a2; i = ((int(*)(int))0x804C32C)(i) )
	{
		int v5 = *(DWORD*)(i + 16);
		p = (SCHAR*)*(DWORD*)(v5 + 76);
		if (p)
		{
			if (p->gm_level || p->empire == a4 || a3[0] == 0x21 )
			{
				if (a3[0] == 0x21)
				{
					a3++;
				}
				p->ChatPacket(6, a3);
			}
		}
	}
	return a3;
}

int ProcessTextTag(SCHAR* a1, char* a2, int a3)
{
	int cos;
	bool cos2;
	int v4;

	const char* temp = "|cfff1e6c0|Htenma:2";

	((void(*)(char*, int, int*, bool*))0x812ABC0)(a2,a3,&cos,&cos2);
	if (!cos2 || cos)
	{
		v4 = a1->CountSpecifyItem(71113);
		char* porw = a2;
		if (a2[0] == 0x21)
		{
			porw++;
		}
		if (!memcmp(temp, porw, 19))
		{
			cos--;
		}
		if (v4 >= cos)
		{
			a1->RemoveSpecifyItem(71113, cos);
			return 1;
		}
	}
	return 0;
}

void HOOK__CHAT(int off)
{
	CallPatch((int)&WhisperLink_asm+3,			IPS(off+0x0812FEA5), 0);
	CallPatch((int)&FuncShout,					IPS(off+0x08134018), 0);
	CallPatch((int)&ProcessTextTag,				IPS(off+0x0812FF34), 0);
	CallPatch((int)&ProcessTextTag,				IPS(off+0x0813072A), 0);

	//printf ( "HOOK__CHAT OK\n");
}
