#include "costumes.h"


// Ten hook dzia?a na wszystkie przedmioty, ale na razie zostawie to tutaj
// Je?eli kiedy? b?dzie potrzeba zrobi? co? opr�cz kostium�w
// Trzeba b?dzie to robic w tym pliku, lub przenie?? te�funkcje do innego pliku
// (Patryk 20.12.2013)

/*This hook works for every item, but I'm puting it in here.
If it will be ever used outside of costumes it will be done here
or this function will have to be moved to its own file */

extern "C" SCHAR* ItemUseEx_hook_c(SCHAR* a1, ITEM* a2, char a3)
{
	item_proto* i = a2->ItemProto;
	if (i->Type == 28)
	{
		equip_costume(a1,a2,i->SubType);
		return 0;
	}
	//else if (i->Type == 18 && i->SubType == 21)
	return a1;
}
extern "C" void ItemUseEx_hook_asm(void)
{
	asm(".intel_syntax noprefix\n"
		".globl ItemUseEx_hook_c\n"
		"add esp, 4\n"
		"mov ecx, dword ptr ds:[0x84C3A38]\n" //  
		"mov edi, [ebp+16]\n"
		"push edi\n"
		"push edx\n"
		"push eax\n"
		"call ItemUseEx_hook_c\n"
		"add esp, 12\n"
		"test eax, eax\n"
		"jz koniec_use_item_ex\n"
		"push 0x08097E98\n"
		"ret\n"

		"koniec_use_item_ex:\n"

		"xor edi, edi\n"
		"push 0x08098047\n"
		"ret\n"
	);
}

extern "C" int expire_item_hook_c(item_proto* a1)
{
	if (a1->Type == 16 || a1->Type == 28)
	{
		return 0x0813A2E0;
	}
	return 0x0813A2C6;
}

extern "C" void expire_item_hook_asm(void)
{
	asm(".intel_syntax noprefix\n"
		".globl expire_item_hook_c\n"
		"add esp, 4\n"
		"mov [esp], eax\n"
		"call expire_item_hook_c\n"
		"push eax\n"
		"mov eax, [ebx+0x44]\n"
		"ret\n"
	);
}

extern "C" int ItemCreateHook(item_proto* i)
{
	if (i->Type == 16 || i->Type == 28 ||
		i->Type == 18 && i->SubType == 21)
	{
		return 0x0813E985;
	}
	return 0x0813E67F;
}

extern "C" void ItemCreate_asm(void)
{
	asm(".intel_syntax noprefix\n"
		".globl ItemCreateHook\n"
		"add esp, 4\n"
		"push edx\n"
		"call ItemCreateHook\n"
		"add esp, 4\n"
		"jmp eax\n"
	);
}

extern "C" int ItemCreateHook2(item_proto* i)
{
	if (i->Type == 16 || i->Type == 28 ||
		i->Type == 18 && i->SubType == 21)
	{
		return 0x0813EA18;
	}
	return 0x0813E72B;
}

extern "C" void ItemCreate_asm2(void)
{
	asm(".intel_syntax noprefix\n"
		".globl ItemCreateHook2\n"
		"add esp, 4\n"
		"push eax\n"
		"call ItemCreateHook2\n"
		"add esp, 4\n"
		"jmp eax\n"
	);
}

extern "C" void SetPart_hook_c(SCHAR* a1, char part, unsigned short id)
{
	if (!a1->lpDesc || a1->isDestroyed)
	{
		*(short*)((int)a1 + 2 * part + 2376) = id;
		return ;
	}
	if (!part) // zbroja /*armour*/
	{
		int id2 = a1->GetQuestFlag("kostium.pm"); //kostium.pm
		if (id2> 1)
		{
			id = id2;
		}
	}
	else if (part == 3)
	{
		int id2 = a1->GetQuestFlag("kostium.phr"); // kostium.phr
		if (id2 > 1)
		{
			id = id2;
		}
	}
	*(short*)((int)a1 + 2 * part + 2376) = id;
}
extern "C" void SetPart_cp(SCHAR* a1, char part, unsigned short id)
{
	return;
}
extern "C" void set_part_hook_asm(void)
{
	asm(".intel_syntax noprefix\n"
		".globl SetPart_hook_c\n"
		"add esp, 4\n"
		"push esi\n"
		"push ebx\n"
		"push edi\n"
		"call SetPart_hook_c\n"
		"add esp, 12\n"
		"push 0x0806AF42\n"
		"ret\n"
	);
}
extern "C" bool CreateCostumeItem(SCHAR* a1, char a2, int a3, char x)
{
	int pos = a1->GetEmptyInventory(a2+1);
	if (pos != -1)
	{
		ITEM_MANAGER* IM = ITEM_MANAGER_SINGLETON;
		ITEM* itm = IM->CreateItem(a3, 1, 0, 0, -1);
		if (itm == 0)
		{
			//a1->ChatPacket(1, "itm = 0 ??");
			return 0;
		}
		int time = 0;
		if (a2) //zbroja
		{
			if (x)
			{
				a1->ChatPacket(5, "cos 1|1|0");
			}
			a1->SetQuestFlag("kostium.pm", 1); // kostium.pm
			time = a1->GetQuestFlag("kostium.m_time"); //kostium.m_time
			a1->SetQuestFlag("kostium.m_time", 1); // kostium.m_time
			ITEM* arm = a1->GetWear(0);
			if (!arm)
			{
				a1->SetPart(0,0);
			}
			else
			{
				a1->SetPart(0, arm->ItemProto->ID);
			}
			a1->RemoveAffect(1002);
		}
		else
		{
			if (x)
			{
				a1->ChatPacket(5, "cos 1|0|0");
			}
			a1->SetQuestFlag("kostium.ph", 1); // kostium.ph
			a1->SetQuestFlag("kostium.phr", 1); // kostium.phr
			time = a1->GetQuestFlag("kostium.h_time"); //kostium.h_time
			a1->SetQuestFlag("kostium.h_time", 1); //kostium.h_time
			a1->SetPart(3, 0);
			a1->RemoveAffect(1003);
		}

		a1->UpdatePacket();
		int tem_time = time;
		itm->socket[2] = tem_time;
		event_cancel(&itm->UniqueExpireEvent);
		itm->StartUniqueExpireEvent();
		itm->AddToSCHAR(a1, pos);
		return 1;
	}
	else
	{
		a1->ChatPacket(1, locale_find("nulleqspc")); // Nie masz miejsca w ekwipunku na zmiane kostiumu -- nulleqspc
		return 0;
	}

}

extern "C" bool unequip_costume(SCHAR* a1, char a2, char x)
{
	int id = 0;
	if (a2)
	{
		id = a1->GetQuestFlag("kostium.pm"); // kostium.pm
	}
	else
	{
		id = a1->GetQuestFlag("kostium.ph"); // kostium.ph
	}
	if (id > 1)
	{
		return CreateCostumeItem(a1,a2,id,x);
	}
	return 0;
}

extern "C" void equip_costume(SCHAR* a1, ITEM* a2, char a3)
{
	if (a2->ItemProto->item_limit[0].LimitValue > a1->level)
	{
		a1->ChatPacket(1, locale_find("nulllvc")); //Masz za ma?y poziom by za?o?y? ten przedmiot -- nulllvc
		return;
	}

	ITEM_MANAGER* IM = ITEM_MANAGER_SINGLETON;

	if (a3) // zbroja
	{
		int id = a1->GetQuestFlag("kostium.pm"); //kostium.pm
		if (id <= 1 || unequip_costume(a1,a3,0)) // Je?eli nic nie jest za?o?one
		{
			a1->SetQuestFlag("kostium.pm", a2->ItemProto->ID); //kostium.pm
			int time = a2->socket[2]; // Czas w sekundach
			a1->SetQuestFlag("kostium.m_time", time); // kostium.m_time
			IM->DestroyItem(a2);
			a1->ChatPacket(5, "cos %d|%d|%d", a2->ItemProto->ID, 1, time); // cos %d|%d|%d
			a1->SetPart(0,0);
			a1->UpdatePacket();

			if (a2->ItemProto->item_apply[0].LimitType && a2->ItemProto->item_apply[0].LimitValue)
			{
				char id = *(char*)(APPLY_INFO+(int)a2->ItemProto->item_apply[0].LimitType);
				a1->AddAffect(1002, id, a2->ItemProto->item_apply[0].LimitValue, 0, time, 0, 0, 0);
			}
			return;
		}
		return;
	}
	//Fryzura
	int id = a1->GetQuestFlag("kostium.ph"); // kostium.ph
	if (id <= 1 || unequip_costume(a1,a3,0)) // Je?eli nic nie jest za?o?one
	{
		a1->SetQuestFlag("kostium.ph", a2->ItemProto->ID); //kostium.ph
		a1->SetQuestFlag("kostium.phr", a2->ItemProto->Value[3]); // kostium.phr
		int time = a2->socket[2]; // Czas w sekundach
		a1->SetQuestFlag("kostium.h_time", time);
		IM->DestroyItem(a2);
		a1->ChatPacket(5, "cos %d|%d|%d", a2->ItemProto->ID, 0, time); // cos %d|%d|%d
		a1->SetPart(3,0);
		a1->UpdatePacket();

		if (a2->ItemProto->item_apply[0].LimitType && a2->ItemProto->item_apply[0].LimitValue)
		{
			char id = *(char*)(APPLY_INFO+(int)a2->ItemProto->item_apply[0].LimitType);
			a1->AddAffect(1003, id, a2->ItemProto->item_apply[0].LimitValue, 0, time, 0, 0, 0);
		}
		return;
	}
	return;
}

void COSTUMES_HOOK(int offset)
{
	CallPatch((int)&ItemUseEx_hook_asm+3, IPS(offset+0x08097E92), 0);
	CallPatch((int)&set_part_hook_asm+3, IPS(offset+0x0806AF03), 0);
	for (int i = 0;i<4;i++)
	{
		CallPatch((int)&SetPart_cp, IPS(0x08079493+offset) + 0x27 * i, 0);
	}

	CallPatch((int)&expire_item_hook_asm+3, IPS(0x0813A2C0+offset), 1);
	CallPatch((int)&ItemCreate_asm+3, IPS(0x0813E97B+offset), 5);
	CallPatch((int)&ItemCreate_asm2+3, IPS(0x0813E721+offset), 0);
}
