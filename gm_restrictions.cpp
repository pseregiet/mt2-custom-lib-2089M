#include "gm_restrictions.h"

/*GM (Game Master) can do to many things and that can be abused by not honest GM
so i did few restrictions, for example selling items to other players, throwing
items on the ground etc. All chat packets here basically say "GM CAN'T DO xxx" */

/*GM of lv 5 can do all these things, because it's the highest privilege and that would be just me*/

extern "C" void CHARACTER__OpenMyshop(SCHAR* a1, int a2, int a3, char a4)
{
	if (a1->gm_level && a1->gm_level != 5)
	{
		a1->ChatPacket(1, "GM NIE MO?E OTWIERA? SKLEPIKU");
		return;
	}
	((void(*)(SCHAR*,int,int,char))0x080789D0)(a1,a2,a3,a4);//CHARACTER::OpenMyShop
}

extern "C" void CHARACTER__ReqSafeboxLoad(SCHAR* a1, const char* a2)
{
	if (a1->gm_level && a1->gm_level != 5)
	{
		a1->ChatPacket(1, "Gm nie mo?e u?ywa? depo");
		return;
	}
	((void(*)(SCHAR*,const char*))0x80708F0)(a1,a2);//CHARACTER::ReqSafeboxLoad
}

extern "C" void CHARACTER__ExchangeStart(SCHAR* a1, SCHAR* a2)
{
	if (a1->gm_level && a1->gm_level !=5)
	{
		if (!a1->can_trade)
		{
			a1->ChatPacket(1, "Nie mo?esz handlowa? ");
			a2->ChatPacket(1, "Nie mo?esz handlowa? z GM");
			return;
		}
	}
	if (a2->gm_level && a2->gm_level !=5)
	{
		if (!a2->can_trade)
		{
			a2->ChatPacket(1, "Nie mo?esz handlowa? ");
			a1->ChatPacket(1, "Nie mo?esz handlowa? z GM");
			return;
		}
	}
	((void(*)(SCHAR*,SCHAR*))0x080FD700)(a1,a2);//CHARACTER::StartExchange
}

extern "C" void CHARACTER__DropItem(SCHAR* a1, int a2, int a3)
{
	if(a1->gm_level && a1->gm_level != 5)
	{
		a1->ChatPacket(1, "GM nie mo?e wyrzuca? przedmiotów");
		return;
	}
	((void(*)(SCHAR*,int,int))0x08094D80)(a1,a2,a3); // CHARACTER::DropItem
}

void HOOK__GM_RESTRICTIONS(void)
{
	JmpPatch((int)&CHARACTER__DropItem, 0x08129F25, 0);
	JmpPatch((int)&CHARACTER__DropItem, 0x08129FA5, 0);

	JmpPatch((int)&CHARACTER__ExchangeStart, 0x0812EF66, 0);

	CallPatch((int)&CHARACTER__OpenMyshop, 0x081298BD, 0);

	CallPatch((int)&CHARACTER__ReqSafeboxLoad, 0x080C40E0, 0);

	//printf("HOOK__GM_RESTRICTIONS OK\n");
}
