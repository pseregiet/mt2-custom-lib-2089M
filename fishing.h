#ifndef __LIB_FISHING_HPP
#define __LIB_FISHING_HPP

#include <cstdlib>
#include "types.h"
#include "SCHAR.h"
#include "ITEM.h"
#include "misc.h"
#include "hook_tools.h"
#include <vector>
#include <string.h>


struct fishing
{
	int id_live;
	int id_dead;
	int id_grill;
	int level;
	int chance;
	int map_index;
	int min_len;
	int max_len;
};

void HOOK__FISHING(int off);

#endif

