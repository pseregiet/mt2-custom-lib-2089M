#ifndef __LIB_DUNGEON_MANAGER_HPP
#define __LIB_DUNGEON_MANAGER_HPP

#include "GameSingleton.h"
#include "DUNGEON.h"

#define DUNGEON_MANAGER_SINGLETON (DUNGEON_MANAGER*)*(DWORD*)(0x84C5B74)

#pragma once
class DUNGEON_MANAGER :
	public GameSingleton < DUNGEON_MANAGER, 0x84C5B74 > 
{
public:
	DUNGEON* Create(int index);
	DUNGEON* Find(long index);
};

#endif


