#ifndef __LIB_SERVER_ATTR_MANAGER_HPP
#define __LIB_SERVER_ATTR_mANAGER_HPP

#include <vector>
#include "SERVER_ATTR.h"
#include "singleton.h"

struct sa_map
{
	char name[50];
	int index;
	int x_size;
	int y_size;
};

#pragma once
class SERVER_ATTR_MANAGER : public Singleton<SERVER_ATTR_MANAGER>
{
public:
	std::vector < SERVER_ATTR* > server_attr_maps;
	std::vector <int> maps;
	SERVER_ATTR_MANAGER(void);
	~SERVER_ATTR_MANAGER(void);

	void Initialize(void);
};

#endif


