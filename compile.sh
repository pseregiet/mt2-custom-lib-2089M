#!/bin/bash


# SERVER_ATTR_MANAGER.cpp SERVER_ATTR.cpp ATTRIBUTE.cpp  
g++ -m32 -O2 -fpermissive -s -Wall -fPIC -Wl,-E -masm=intel -I/usr/local/include -L/usr/local/lib -shared -o preload_new.so -w \
hook_tools.cpp \
SCHAR.cpp ITEM.cpp \
ITEM_MANAGER.cpp \
SCHAR_MANAGER.cpp \
SECTREE_MANAGER.cpp \
main.cpp \
ip_secure_main.cpp \
commands.cpp \
character_hooks.cpp \
costumes.cpp pets.cpp \
quests.cpp \
LUA.cpp \
QUEST_MANAGER.cpp \
ENTITY.cpp \
SECTREE.cpp \
DUNGEON.cpp \
DUNGEON_MANAGER.cpp \
small_hooks.cpp \
fishing.cpp pvp.cpp \
DESC.cpp \
chat.cpp \
shop.cpp \
sql.cpp \
DB_MANAGER.cpp