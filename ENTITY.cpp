#include "ENTITY.h"

void ENTITY::ViewReencode(void)
{
	((void(*)(ENTITY*))0x080FB590)(this);
}

void ENTITY::PacketAround(const void* a1, int a2, ENTITY* a3)
{
	((void(*)(ENTITY*, const void*, int, ENTITY*))0x080FB2B0)(this,a1,a2,a3);
}

bool ENTITY::IsType(int a1)
{
	return ((bool(*)(ENTITY*, int))0x080FAE20)(this,a1);
}
