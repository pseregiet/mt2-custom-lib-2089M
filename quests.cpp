#include "quests.h"
/*
This server uses LUA to do quests, but you can do a lot of stuff with that
Here are my own functions that I can call from those quests. I hooked a function
pc.get_gold(). This function doesn't take any arguments normally, and if you don't give it
anything, it will return the player's gold, however, if you give it a number
it will call my own function. This is perhaps the first hook I've done
and I was just adding new functions to it. Now I know how to add my own functions
the proper way...
*/
extern "C" void LuaHook_c(LUA* lua)
{
	QUEST_MANAGER* x = QUEST_MANAGER_SINGLETON;

	if (!lua->isNumber(1))
	{
		
		SCHAR* pc = x->GetPC();
		lua->pushnumber(pc->gold);
		return;
	}
	long double _v0 = lua->tonumber(1);
	int v0 = (int)_v0;

	switch(v0)
	{
	case 1:
		{
			/* SAVE AND UPDATE OF ITEM */
			//SAVE i UPDATE ITEM�W
			ITEM* item = x->GetITEM();
			if (item)
			{
				item->UpdatePacket();
				item->Save();
			}
			break;
		}

	case 2:
		{
			/*GET_BONUS_ID*/
			// POBIERANIE ID BONU
			ITEM* item = x->GetITEM();
			if (item)
			{
				long double v1 = lua->tonumber(2);
				int bon = item->bonus[(int)v1].id;
				lua->pushnumber(bon);
			}
			break;
		}

	case 3:
		{
			/*GET_BONUS_VALUE*/
			//POBIERANIE VAL BONU
			ITEM* item = x->GetITEM();
			if (item)
			{
				long double v1 = lua->tonumber(2);
				int bon = item->bonus[(int)v1].val;
				lua->pushnumber(bon);
			}
			break;
		}

	case 4:
		{
			/*GET_MOB_COUNT_IN_DUNGEON*/
			//POBIERA ILO� MOB�W W DUNGEONIE
			DUNGEON* dung = x->GetDungeon();
			if (dung)
			{
				lua->pushnumber(*(DWORD*)(dung + 120));
			}
			break;
		}

	case 5:
		{
			/*GET_NPC_LEVEL*/
			//POBIERANIE LV NPC
			SCHAR* npc = x->GetNPC();
			if (npc)
			{
				lua->pushnumber((int)npc->level);
			}
			break;
		}

	case 6:
		{
			/*NEW_BONUS_STRONG_AGAINST....(TYPES OF MONSTERS)*/
			//NOWE BONUSY SILNY PRZECIW
			SCHAR* a1 = x->GetPC();
			if (a1)
			{
				long double arg[3];
				for (int i = 0;i<3;i++)
				{
					arg[i] = lua->tonumber(i);
				}
				a1->AddAffect(515, (char)arg[0], (int)arg[1], 0, (int)arg[2], 0, 0, 0);
			}
			break;
		}

	case 7:
		{
			/*SET_SKILL_POINTS*/
			//USTAWIANIE PUNKT�W UM
			SCHAR* pc = x->GetPC();
			if (pc)
			{
				long double v1 = lua->tonumber(2);
				pc->SetPoint(28, (int)v1);
			}
			break;
		}

	case 8:
		{
			/*GET_SKILL_POINTS*/
			//POBIERANIE PUNKT�W UM
			SCHAR* pc = x->GetPC();
			if (pc)
			{
				int ppi = pc->GetPoint(28);
				lua->pushnumber((double)ppi);
			}
			break;
		}

	case 9:
		{
			/*SET FREE (CAN ATTACK ANYONE) PK MODE*/
			//USTAWIA TRYB WOLNY PK
			SCHAR* pc = x->GetPC();
			if (pc)
			{
				pc->SetPKMode(2);
			}
			break;
		}

	case 10:
		{
			/*SPAWN_COWARD_MOB_IN_DUNGEON (NORMALLY ALL MOBS IN DUNGEONS ARE AGRO, COWARDS RUN AWAY FROM YOU EVEN IF YOU ATTACK)*/
			//RESPI MOBA TCHURZA DLA DUNG.
			DUNGEON* dung = x->GetDungeon();
			if (dung)
			{
				long double arg[4];
				for (int i = 0;i<4;i++)
				{
					arg[i] = lua->tonumber(i);
				}
				SCHAR* a1 = dung->SpawnMob((int)arg[0], (int)arg[1], (int)arg[2]);
				a1->SetCoward();
			}
			break;
		}

	case 11:
		{
			/*CREATES DUNGEON, WARPS PLAYER IN AND RETURNS INDEX OF THE MAP*/
			//TWORZY DUNGEON, PRZENOSI GRACZA I ZWRACA INDEX MAPY DO LUA
			DUNGEON_MANAGER* dm = DUNGEON_MANAGER_SINGLETON;

			long double v1 = lua->tonumber(2);
			DUNGEON* dung = dm->Create((int)v1);

			int map = dung->GetMapIndex();
			lua->pushnumber(map);
		}
	case 12:
		{
			/*JOINS TO THE DUNGEON WITH GIVEN MAP INDEX*/
			//DO��CZA SI� DO DUNGEONA O PODANYM IDX MAPY
			DUNGEON_MANAGER* dm = DUNGEON_MANAGER_SINGLETON;

			long double v1 = lua->tonumber(2);
			DUNGEON* dung = dm->Find((long)v1);
			if (dung)
			{
				SCHAR* pc = x->GetPC();
				if (pc)
				{
					dung->Join(pc);
				}
			}
			break;
		}

	case 13:
		{
			/*FUNCTION FOR TEAM PVP*/
			//FUNKCJA OD TEAM PVP
			long double _v1 = lua->tonumber(2);
			long double _v2 = lua->tonumber(3);

			int v1 = (int)_v1;
			int v2 = (int)_v2;

			SCHAR_MANAGER* cm = SCHAR_MANAGER_SINGLETON;

			SCHAR* a1 = x->GetPC();
			SCHAR* a2 = cm->Find(v2);

			//DUNGEON_MANAGER* dm;
			//dm = dm->instance();
			DUNGEON_MANAGER* dm = DUNGEON_MANAGER_SINGLETON;

			DUNGEON* dung = dm->Create(v1);
			if (dung)
			{
				if (a1->party && a2->party)
				{
					dung->JoinPartyOnPosition(a1->party, 10, 10);
					dung->JoinPartyOnPosition(a1->party, 20, 20);
				}
			}
			break;
		}

	case 14:
		{
			/*SETS QUEST FLAG FOR BLACKSMITH IN DEVILS TOWER (TO MUCH TO EXPLAIN)*/
			// USTAWIAMA FLAGE OD KOWALA DT
			DUNGEON* dung = x->GetDungeon();
			if (dung)
			{
				std::string a2 = "deviltower_zone.can_refine"; // deviltower_zone.can_refine
				int a3 = (int)lua->tonumber(2);
				dung->SetFlag(a2, a3);
			}
			break;
		}

	case 15:
		{
			/*CREATES ITEM AND SETS IT AS A CURRENT QUEST ITEM, SO YOU CAN SPAWN ITEM AND USE item.xx FUNCTIONS ON IT*/
			// TWORZY ITEM I USTAWIA GO JAKO ITEM CURRENT QUESTA
			SCHAR* pc = x->GetPC();
			if (pc)
			{
				int v1 = (int)lua->tonumber(2);
				int v2 = (int)lua->tonumber(3);

				ITEM* i = pc->AutoGiveItem(v1,v2,-1,1);
				pc->quest_item_ptr = i;
			}
			break;
		}

	case 16:
		{
			/*CHECKS IF GIVEN AFFECT IS ALREADY ON A PLAYER*/
			// SPRAWDZA CZY PODANY AFFEKT JEST NA�O�ONY NA GRACZA
			SCHAR* pc = x->GetPC();
			int v1 = (int)lua->tonumber(2);
			int i = pc->FindAffect(v1, 0);
			lua->pushbool(i);
			break;
		}

	case 17:
		{
			/*RETURNS NUMBER OF PLAYERS IN PARTY*/
			//ZWRACA ILOSC OSOB W PT NA TYM SAMYM GAME
			SCHAR* pc = x->GetPC();
			char ret = 0;
			if (pc->party)
			{
				ret = pc->party->MemberCount();
			}
			lua->pushnumber(ret);
			break;
		}

	case 18:
		{
			/*RETURNS TRUE IF SOMEONE IN THE PARTY HAS BLOCKED ENTRANCE TO A DUNGEON*/
			//ZWRACA TRUE JE�ELI KTO� MA ZABLOKOWANY DOST�P DO DUNGA PRZEZ QF
			SCHAR* pc = x->GetPC();
			bool ret;
			char* qf = lua->tostring(2);
			if (pc->party)
			{
				ret = 1; pc->party->CheckQF(qf);
			}
			else
			{
				ret = ((DWORD)pc->GetQuestFlag(qf) < get_global_time());
			}
			lua->pushbool(ret);
			break;
		}

	case 19:
		{
			/*RETURNS VID (VIRTUAL ID) OF NPC*/
			//ZWRACA VIC NPC
			SCHAR* npc = x->GetNPC();
			if (npc)
			{
				lua->pushnumber((double)npc->vid);
			}
			break;
		}

	case 20:
		{
			/*NOTICE FOR TEAN PVP*/
			//NOTICE DLA TEAM PVP
			SCHAR* killer = x->GetPC();
			SCHAR* victim = x->GetNPC();

			char q[100];
			sprintf(q, locale_find("pck%dpc"), killer->GetName(), victim->GetName()); //Gracz %s zabi� gracza %s -- pck%dpc
			SendNoticeMap(q, killer->map_index, 0);
			lua->pushnumber(victim->vid);
			break;
		}

	case 21:
		{
			/*RETURNS NUMBER OF ALIVE PLAYERS IN PARTY*/
			//ZWRACA ILOSC ZYWYCH OSOB W PT
			SCHAR* pc = x->GetPC();
			if (pc->party)
			{
				int x = pc->party->LiveCountOnMap(pc->map_index);
				lua->pushnumber(x);
			}
			break;
		}

	case 22:
		{
			/*RANDOMISES BONUSES IN ITEM*/
			//ZMIENIA BONY W ITEMIE
			ITEM* item = x->GetITEM();
			if (item)
			{
				item->ChangeAttribute(0);
			}
			break;
		}

	case 23:
		{
			/*GIVE COSTUMES BONUSES*/
			//NADAJE BONUSY KOSTIUM�W
			SCHAR* pc = x->GetPC();
			pc->RefreshCostumeBonuses();
			break;
		}

	case 24:
		{
			//QUERY ZWRACA TABELE

			/*int *result, *p_res, i=0;
			MYSQL_FIELD* field;
			char *query, **row;
			unsigned long long int num = 0;

			query = lua->tostring(2);
			//DB_MANAGER* dbm = dbm->instance();

			result = (int*)dbm->DirectQuery(query);
			p_res = *(int**)result[3];

			if (!*p_res)
			{
				lua->pushnumber(0);
				return;
			}
			lua->newtable();
			while ((row = mysql_fetch_row(*p_res)))
			{
				while ( (field = ( (MYSQL_FIELD*)mysql_fetch_field(*p_res) )))
				{
					lua->pushstring(field->name);
					if (sscanf(row[i], "%llu", &num))
					{
						lua->pushnumber(num * 1.0);
					}
					else
					{
						lua->pushstring(row[i]);
					}
					lua->rawset(-3);
					i++;
				}
			}*/
			break;
		}
	case 25:
		{
			/*SUMMON PET*/
			// PRZYWOLANIE PETA

			SCHAR* player = x->GetPC();
			if (!player->pet)
			{
				// tutaj dodac sprawdzanie wytrzymalosci jezeli cos takiego ma byc

				SCHAR_MANAGER* CM = SCHAR_MANAGER_SINGLETON;
				int mobvnum = lua->tonumber(2);
				std::string stri = ("pet.level"); //pet.level
				int pet_level = player->GetQuestFlag(stri);
				char newname[24];
				const char* base_name = "%s Pet lv%d"; //%s Pet lv%d
				sprintf(newname, base_name, player->GetName(), pet_level);
				int spawn_map = player->map_index;
				int spawn_x = player->x;
				int spawn_y = player->y;
				SCHAR* mobid = CM->SpawnMob(mobvnum, spawn_map, spawn_x, spawn_y, 0, 0, -1, 0);
				if (!mobid)
				{
					return ;
				}
				player->SetQuestFlag("pet.is_summon", mobvnum); // pet.is_summon
				pet_name_asm(mobid, newname, strlen(newname));

				mobid->rider = player;
				player->pet = mobid;
				mobid->level = pet_level;

				mobid->Show(spawn_map, spawn_x, spawn_y,0,0);
				lua->pushbool(1);
			}
			else
			{
				lua->pushbool(0);
			}
			break;
		}
	case 26:
		{
			/*UNSUMMON PET*/
			// ODWOLANIE PETA

			SCHAR* player = x->GetPC();
			SCHAR* pet = player->pet;
			if (pet)
			{
				pet->rider = 0;
				player->SetQuestFlag("pet.is_summon", 0); // pet.is_summon
				PET_Destroy(pet);

				player->pet = 0;
			}
			break;
		}
	case 27:
		{
			/*CREATES DUNGEON AND WARPS WHOLE PARTY IN (TAKES EXACT COORDINANTES, USUALLY IT JUST WARPS TO THE FIXED PLACE IN MAP)*/
			// TWORZY DUNGEON I PRZENOSI PARTY (KORDY)

			SCHAR* pc = x->GetPC();
			if (!pc->party)
			{
				lua->pushbool(0);
				break;
			}
			DUNGEON_MANAGER* DM = DUNGEON_MANAGER_SINGLETON;

			int map = lua->tonumber(2);
			int x = lua->tonumber(3);
			int y = lua->tonumber(4);

			DUNGEON* dung = DM->Create(map);
			if (dung)
			{
				dung->JoinPartyOnPosition(pc->party, x, y);
				lua->pushbool(1);
			}
			else
			{
				lua->pushbool(0);
			}
			break;
		}
	case 28:
		{
			/*RETURNS NUMBER OF MOBS WITH GIVEN ID ON MAP*/
			// LICZY MOBY O DANYM ID NA MAPIE

			SCHAR* pc = x->GetPC();
			int map = pc->map_index;
			int vnum = lua->tonumber(2);

			SECTREE_MANAGER* SM = SECTREE_MANAGER_SINGLETON;
			int mobs = SM->GetMonsterCountInMap(map, vnum);
			lua->pushnumber(mobs);
			break;
		}
	}
}

extern "C" void LuaHook_asm(void)
{
	asm(
	".intel_syntax noprefix\n"
	".globl LuaHook_c\n"
	"push ebx\n"
	"call LuaHook_c\n"
	"add esp, 4\n"
	);
}

void HOOK__QUESTS(int offset)
{
	CallPatch((int)&LuaHook_asm, IPS(offset+0x081A3485), 0);

	//printf("HOOK__QUESTS OK\n");
}
