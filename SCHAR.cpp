#include "SCHAR.h"

char* SCHAR::GetName(void)
{
	return ((char*(*)(SCHAR*))0x08069E70)(this);
}
int SCHAR::CreateFly(char a1, SCHAR* a2)
{
	return ((int(*)(SCHAR*,char, SCHAR*))0x080835B0)(this,a1, a2);
}
void SCHAR::ChatPacket(char a1, const char* a2, ...)
{
	//((void(*)(SCHAR*,char,const char*,...))0x806C9E0)(this,a1,a2);

	va_list va;
	va_start(va, a2);
	//if (this->lpDesc)
	//{
	//	if (a2)
	//	{
	char s[0x201];
		//	char x[9];
	int v7 = vsnprintf(s, 0x201u, a2, va);
	va_end(va);

			/**(char*)x = 4;
			*(short*)(x+1) = v7 + 9;
			*(char*)(x+3) = a1;
			*(int*)(x+4) = 0;
			*(char*)(x+8) = 1; // DESC::GetEmpire

			TEMP_BUFFER tb(2000, 0);
			tb.write(x, 9);
			tb.write(s, v7);

			int v24 = tb.size();
			const void* v25 = tb.read_peek();

			this->lpDesc->Packet(v25, v24);

		}
	}*/
	((void(*)(SCHAR*,char,const char*,...))0x806C9E0)(this,a1,s);
	
}
int SCHAR::GetRaceNum(void)
{
	return ((int(*)(SCHAR*))0x8069EF0)(this);
}
int SCHAR::SetRider(SCHAR* a1)
{
	return ((int(*)(SCHAR*,SCHAR*))0x08090500)(this,a1);
}
int SCHAR::GetQuestFlag(std::string a1)
{
	return ((int(*)(SCHAR*,std::string))0x80711C0)(this,a1);
}
int SCHAR::Show(int a1, int a2, int a3, int a4, int a5)
{
	return ((int(*)(SCHAR*,int, int, int, int, int))0x8070A80)(this,a1, a2, a3, a4, a5);
}
int SCHAR::Stun(void)
{
	return ((int(*)(SCHAR*))0x08088750)(this);
}
int SCHAR::AddAffect(int a1, char a2, int a3, int a4, int a5, int a6, char a7, char a8)
{
	return ((int(*)(SCHAR*,int, char, int, int, int, int, char, char))0x8082370)(this,a1, a2, a3, a4, a5, a6, a7, a8);
}
int SCHAR::SetWarp(int a1, int a2, int a3)
{
	return ((int(*)(SCHAR*,int, int, int))0x80757E0)(this,a1, a2, a3);
}
int SCHAR::SetQuestFlag(std::string a1, int a2)
{
	return ((int(*)(SCHAR*,std::string, int))0x08071120)(this,a1, a2);
}
int SCHAR::ChangePoint(char a1, int a2, char a3, char a4)
{
	return ((int(*)(SCHAR*,char, int, char, char))0x8075FB0)(this,a1, a2, a3, a4);
}
int SCHAR::BeginFight(SCHAR *  a1)
{
	return ((int(*)(SCHAR*,SCHAR * ))0x08083940)(this,a1);
}
int SCHAR::CountSpecifyItem(int a1)
{
	return ((int(*)(SCHAR*,int))0x8091BC0)(this,a1);
}
void SCHAR::RemoveSpecifyItem(int a1, int a2)
{
	return ((void(*)(SCHAR*,int, int))0x80946E0)(this,a1, a2);
}
int SCHAR::RemoveAffect(int a1)
{
	return ((int(*)(SCHAR*,int))0x8081800)(this,a1);
}
int SCHAR::GetPoint(unsigned char a1)
{
	return ((int(*)(SCHAR*,unsigned char))0x0806BD10)(this,a1);
}
void SCHAR::SetPoint(unsigned char a1, int a2)
{
	return ((void(*)(SCHAR*,unsigned char, int))0x0806F770)(this,a1, a2);
}
void SCHAR::SetPKMode(char a1)
{
	return ((void(*)(SCHAR*,char))0x8083230)(this,a1);
}
void SCHAR::SetCoward(void)
{
	return ((void(*)(SCHAR*))0x80BDFC0)(this);
}
bool SCHAR::IsDead(void)
{
	return ((bool(*)(SCHAR*))0x08082F20)(this);
}
int SCHAR::CountItem(int a1)
{
	return ((int(*)(SCHAR*,int))0x8091BC0)(this,a1);
}
void SCHAR::UnEquipRideItem(void)
{
	return ((void(*)(SCHAR*))0x8092360)(this);
}
char SCHAR::RequestParty(SCHAR* a1)
{
	return ((char(*)(SCHAR*,SCHAR*))0x807C880)(this,a1);
}
char SCHAR::RequestPartyAccept(SCHAR* a1)
{
	return ((char(*)(SCHAR*,SCHAR*))0x806EE20)(this,a1);
}
char SCHAR::PartyJoin(SCHAR* a1)
{
	return ((char(*)(SCHAR*,SCHAR*))0x806CEA0)(this,a1);
}
char SCHAR::PartyInvite(SCHAR* a1)
{
	return ((char(*)(SCHAR*,SCHAR*))0x807C660)(this,a1);
}
char SCHAR::GetPKMode(void)
{
	return ((char(*)(SCHAR*))0x08083070)(this);
}
void SCHAR::GoTo(int a1, int a2)
{
	return ((void(*)(SCHAR*,int, int))0x08073D50)(this,a1, a2);
}
ITEM* SCHAR::GetWear(char a1)
{
	return ((ITEM*(*)(SCHAR*,char))0x80919D0)(this,a1);
}
ITEM* SCHAR::GetItem(int a1)
{
	return ((ITEM*(*)(SCHAR*,int))0x8091B60)(this,a1);
}
ITEM* SCHAR::AutoGiveItem(int a1, unsigned char a2, int a3, char a4)
{
	return ((ITEM*(*)(SCHAR*,int, unsigned char, int, char))0x08095270)(this,a1, a2, a3, a4);
}
int SCHAR::FindAffect(int a1, char a2)
{
	return ((int(*)(SCHAR*,int, char))0x08080F40)(this,a1, a2);
}
int SCHAR::SetPart(char a1, short a2)
{
	return ((int(*)(SCHAR*,char, short))0x0806AEE0)(this,a1, a2);
}
int SCHAR::UpdatePacket(void)
{
	return ((int(*)(SCHAR*))0x0806BA90)(this);
}
int SCHAR::GetEmptyInventory(int a1)
{
	return ((int(*)(SCHAR*,int))0x08091600)(this,a1);
}
void SCHAR::ComputePoints(void)
{
	return ((void(*)(SCHAR*))0x08079210)(this);
}
void SCHAR::PointsPacket(void)
{
	return ((void(*)(SCHAR*))0x0806BE40)(this);
}
short SCHAR::GetPart(char a1)
{
	return ((short(*)(SCHAR*,char))0x0806ADF0)(this,a1);
}
void SCHAR::MoveItem(char a1, char a2, unsigned char a3)
{
	return ((void(*)(SCHAR*,char, char, unsigned char))0x08096700)(this,a1, a2, a3);
}
int SCHAR::SwapItem(char a1, char a2)
{
	return ((int(*)(SCHAR*,char, char))0x8092040)(this,a1, a2);
}
int SCHAR::GetJob(void)
{
	return ((int(*)(SCHAR*))0x806C820)(this);
}
bool SCHAR::IsRiding(void)
{
	return ((bool(*)(SCHAR*))0x806E0B0)(this);
}
int SCHAR::GerRealPoint(BYTE a1)
{
	return ((int(*)(SCHAR*,BYTE))0x806A270)(this,a1);
}
void SCHAR::SetSkillLevel(int a1, int a2)
{
	return ((void(*)(SCHAR*,int, int))0x80AC6A0)(this,a1, a2);
}
void SCHAR::SkillLevelPacket(void)
{
	return ((void(*)(SCHAR*))0x80AC980)(this);
}
int SCHAR::FishTake(void)
{
	return ((int(*)(SCHAR*))0x806D030)(this);
}
int SCHAR::Fishing(void)
{
	return ((int(*)(SCHAR*))0x08070CA0)(this);
}
int SCHAR::GetSkillLevel(int a1)
{
	return ((int(*)(SCHAR*,int))0x80AC5D0)(this,a1);
}



void SCHAR::ActEmotion(const char* emotion)
{
	TEMP_BUFFER* tb = new TEMP_BUFFER(2000, 0);

	char x[9];
	char s[100];

	short len = sprintf(s, "%s %u %u", emotion, this->vid, 0);

	*(char*)x = 4;
	*(short*)(x+1) = len + 10;
	*(char*)(x+3) = 5;
	*(int*)(x+4) = 0;
	*(char*)(x+8) = 0x29;

	tb->write(x, 9);
	tb->write(s, len+1);

	int size = tb->size();
	const void* p = tb->read_peek();

	this->PacketAround(p, size, 0);
	delete tb;
}

void SCHAR::SaveExitLocation(void)
{
	((void(*)(SCHAR*))0x806DD80)(this);
}



void SCHAR::CreateFlyOnlyForMe(char type, SCHAR* target)
{
	char packet[10];
	packet[0] = 70;
	packet[1] = type;
	*(DWORD*)(packet+2) = this->vid;
	*(DWORD*)(packet+6) = target->vid;
	this->lpDesc->Packet((const void*)packet, 10);
}



bool SCHAR::IsStun(void)
{
	return ((bool(*)(SCHAR*))0x8082EE0)(this);
}

void SCHAR::ChatToEveryone(const char* msg, char type)
{
	TEMP_BUFFER* tb = new TEMP_BUFFER(2000, 0);

	char x[9];
	char s[100];

	short len = sprintf(s, "%s", msg);

	*(char*)x = 4;
	*(short*)(x+1) = len + 10;
	*(char*)(x+3) = 5;
	*(int*)(x+4) = 0;
	*(char*)(x+8) = 0x29;

	tb->write(x, 9);
	tb->write(s, len+1);

	int size = tb->size();
	const void* p = tb->read_peek();

	this->PacketAround(p, size, 0);
	delete tb;
}

void SCHAR::StartRecoveryEvent(void)
{
	((void(*)(SCHAR*))0x807C590)(this);
}

void SCHAR::SetPosition(int a1)
{
	((void(*)(SCHAR*,int))0x8074B40)(this,a1);
}

void SCHAR::ExitToSavedLocation(void)
{
	((void(*)(SCHAR*))0x8075EC0)(this);
}

void SCHAR::ReviveInvisible(int a1)
{
	((void(*)(SCHAR*,int))0x0806C190)(this,a1);
}

void SCHAR::RefreshCostumeBonuses(void)
{
	int main = this->GetQuestFlag("kostium.pm");
	if (main)
	{
		int time_main = this->GetQuestFlag("kostium.m_time");

		ITEM_MANAGER* im = ITEM_MANAGER_SINGLETON;

		item_proto* a3 = im->GetTable(main);
		if (a3)
		{
			if (a3->item_apply[0].LimitType && a3->item_apply[0].LimitValue)
			{
				char id = *(char*)(APPLY_INFO + a3->item_apply[0].LimitType);
				this->AddAffect(1002, id, a3->item_apply[0].LimitValue, 0, time_main, 0, 0, 0);
			}
		}
	}

	int hair = this->GetQuestFlag("kostium.ph");
	if (main)
	{
		int hair_main = this->GetQuestFlag("kostium.h_time");

		ITEM_MANAGER* im = ITEM_MANAGER_SINGLETON;

		item_proto* a3 = im->GetTable(hair);
		if (a3)
		{
			if (a3->item_apply[0].LimitType && a3->item_apply[0].LimitValue)
			{
				char id = *(char*)(APPLY_INFO + a3->item_apply[0].LimitType);
				this->AddAffect(1003, id, a3->item_apply[0].LimitValue, 0, hair_main, 0, 0, 0);
			}
		}
	}
}
