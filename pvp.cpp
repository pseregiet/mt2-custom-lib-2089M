#include "pvp.h"

/*Stanrad PVP system looks like this :

player X sends pvp request
player Y accepts
PVP starts immediately

It is very frustating, so I added a countdown after accepting*/

extern "C" char pvp_hook_c(SCHAR* a1, int vid, int pvp)
{
	
	int now_time = get_global_time();
	int set_time = a1->pvp_time;
	if (now_time < set_time)
	{
		a1->ChatPacket(1, "i feel haxs"); //i feel haxs
		return 0;
	}
	if (now_time - set_time > 120)
	{
		a1->ChatPacket(1, "Czas minal !"); // Czas minal !
		return 0;
	}
	return ((char(*)(int,int))0x8173B90)(pvp,vid);
}

extern "C" void pvp_hook_asm(void)
{
	asm(".intel_syntax noprefix\n"
		".globl pvp_hook_c\n"
		"push edx\n"
		"push eax\n"
		"push edi\n"
		"call pvp_hook_c\n"
		"add esp, 12\n"
		"ret\n"
	);
}

extern "C" void pvp_hook_new_c(SCHAR* wyzywajacy, SCHAR* ofiara)
{
	ofiara->pvp_agree_time = get_global_time()+120;
	char q[15];
	sprintf(q, "pvp_ask %d", wyzywajacy->vid); //pvp_ask %d
	ofiara->ChatPacket(5, q);
}

extern "C" void pvp_hook_new_asm(void)
{
	asm(".intel_syntax noprefix\n"
		".globl pvp_hook_new_c\n"
		"add esp, 4\n"
		"mov eax, [ebp+16]\n" // ofiara
		"push eax\n"
		"mov eax, [ebp+12]\n" // wyzywajacy
		"push eax\n"
		"call pvp_hook_new_c\n"
		"add esp, 8\n"
		"mov dword ptr [esp], 0x24\n"
		"mov eax, 0x0817434E\n"
		"jmp eax\n"
	);
}

/* In standard PVP system, after you lose PVP, you can get your "revenge"
You click revenge and the PVP starts immediately without accepting from another player
I remove this feature totally*/

extern "C" void pvp_no_revenge_asm(void)
{
	asm(".intel_syntax noprefix\n"
		"add	esp, 4\n"
		"mov	eax, 0x8173660\n" // cpvp::win()
		"call	eax\n"
		"mov	eax, [ebp+8]\n"
		"push	esi\n"
		"push	eax\n"
		"mov	eax, 0x08173BC0\n" // cpvp::delete()
		"call	eax\n"
		"add	esp, 8\n"
		"push	0x08173B7C\n"
		"ret\n"
	);
}

extern "C" char pvp_hook_decline_c(int pvp_instance)
{
	if (((bool(*)(int))0x08172C20)(pvp_instance))
	{
		return 1;
	}
	return 0;
}

extern "C" void pvp_hook_decline_asm(void)
{
	asm(".intel_syntax noprefix\n"
		".globl pvp_hook_decline_c\n"
		"add esp, 4\n"
		"push esi\n"
		"call pvp_hook_decline_c\n"
		"add esp, 4\n"
		"test eax, eax\n"
		"jnz nie_wylaczaj\n"
		"mov dword ptr [esp+4], 1\n"
		"mov eax, 0x81741E8\n"
		"jmp eax\n"

		"nie_wylaczaj:\n"

		"push 0x081741C6\n"
		"ret\n"
	);
}

extern "C" void HOOK_PVP(int off)
{
	CallPatch((int)&pvp_hook_asm+3,				IPS(off+0x081742CD), 0); //
	CallPatch((int)&pvp_hook_new_asm+3,			IPS(off+0x08174347), 2);
	CallPatch((int)&pvp_no_revenge_asm+3,		IPS(off+0x08173B77), 0);
	CallPatch((int)&pvp_hook_decline_asm+3,		IPS(off+0x081741E0), 3);

	unsigned char patch7 = 1;
	patch(IPS(off+0x08173A50), &patch7, 1);
	unsigned char patch8[3] = {0xC0, 0xD4, 0x01};
	patch(IPS(off+0x081741C0), patch8, 3);

	//printf("PVP_HOOK OK !\n");
}
