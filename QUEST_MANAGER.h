#ifndef __LIB_QUEST_MANAGER_HPP
#define __LIB_QUEST_MANAGER_HPP


#include "GameSingleton.h"
#include "SCHAR.h"
#include "types.h"
#include "DUNGEON.h"
#include <string>

#define QUEST_MANAGER_SINGLETON (QUEST_MANAGER*)*(DWORD*)(0x84C4860)

#pragma once
class QUEST_MANAGER : public GameSingleton <QUEST_MANAGER, 0x84C4860 >
{
public:


	SCHAR* GetPC(void);
	int GetEventFlag(std::string flag);
	void SetEventFlag(std::string flag, int value);
	DUNGEON* GetDungeon(void);
	SCHAR* GetNPC(void);
	ITEM* GetITEM(void);
};

#endif


