#include "SECTREE_MANAGER.h"


SECTREE_MAP* SECTREE_MANAGER::GetMap(long a1)
{
	return ((SECTREE_MAP*(*)(SECTREE_MANAGER*,long))0x081CB110)(this,a1);
}

SECTREE_MAP* SECTREE_MANAGER::Get(DWORD a1, DWORD a2, DWORD a3)
{
	return ((SECTREE_MAP*(*)(SECTREE_MANAGER*,DWORD,DWORD,DWORD))0x081CB1C0)(this,a1,a2,a3);
}

bool SECTREE_MANAGER::GetRecallPositionByEmpire(int a1, BYTE a2, int* a3)
{
	return ((bool(*)(SECTREE_MANAGER*,int,BYTE,int*))0x081CC7A0)(this,a1,a2,a3);
}

int SECTREE_MANAGER::GetMonsterCountInMap(long map, DWORD vnum)
{
	return ((int(*)(SECTREE_MANAGER*,long,DWORD))0x081CD880)(this,map,vnum);
}

