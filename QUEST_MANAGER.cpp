#include "QUEST_MANAGER.h"




SCHAR* QUEST_MANAGER::GetPC(void)
{
	return (SCHAR*)(*(DWORD*)(this+220));
}


int QUEST_MANAGER::GetEventFlag(std::string flag)
{
	return ((int(*)(QUEST_MANAGER*, std::string))0x081A7950)(this,flag); 
}


void QUEST_MANAGER::SetEventFlag(std::string flag, int value)
{
	((void(*)(QUEST_MANAGER*,std::string,int))0x081B0160)(this,flag,value);
}


DUNGEON* QUEST_MANAGER::GetDungeon(void)
{
	return ((DUNGEON*(*)(QUEST_MANAGER*))0x081A6F20)(this);
}


SCHAR* QUEST_MANAGER::GetNPC(void)
{
	return ((SCHAR*(*)(QUEST_MANAGER*))0x081A6890)(this);
}


ITEM* QUEST_MANAGER::GetITEM(void)
{
	return ((ITEM*(*)(QUEST_MANAGER*))0x081A6FC0)(this);
}
