#ifndef __LIB_ATTRIBUTE_HPP
#define __LIB_ATTRIBUTE_HPP

#include "types.h"
#include "SERVER_ATTR.h"

class SERVER_ATTR;

#pragma once
class ATTRIBUTE
{
public:
	int dataType;
    DWORD defaultAttr;
    DWORD width;
    DWORD height;
    void *data;
    BYTE **bytePtr;
    WORD **wordPtr;
    DWORD **dwordPtr;

	int map_index;

};

#endif


