#include "SERVER_ATTR.h"

SERVER_ATTR::SERVER_ATTR(void)
{
	this->x_size = 0;
	this->y_size = 0;
}
bool SERVER_ATTR::Initialize(const char* map_name, int x, int y)
{
	char buf[100];

	for (int i = 0; i<y; i++)
	{
		for (int j = 0; j<x; j++)
		{
			sprintf(buf, "locale/germany/map/%s/sattr/00%d00%d/attr.atr", map_name, i, j);

			FILE* f;
			f = fopen(buf, "rb");
			if (f == NULL)
			{
				//printf("Blad przy czytaniu %s\n", buf);
				((void(*)(const char*,int,const char*,...))sys_err)("SERVER_ATTR", 13, "Blad przy czytaniu %s\n", buf);
				return false;
			}
			fseek(f, 0, SEEK_END);
			long lSize = ftell(f);
			rewind(f);

			if (lSize != 65542)
			{
				((void(*)(const char*,int,const char*,...))sys_err)("SERVER_ATTR", 13, "Zly rozmiar attr ! (%s)\n", buf);
				return false;
			}

			char* temp = new char[65542];
			int result = fread(temp, 1, 65542, f);

			if (result != lSize)
			{
				((void(*)( const char*,int,const char*,...))sys_err)("SERVER_ATTR", 13, "Blad przy czytaniu fread(%s)\n", buf);
				return false;
			}
			fclose(f);
			this->attrs.push_back(temp);
		}
	}
	this->x_size = x;
	this->y_size = y;
	return true;
}


SERVER_ATTR::~SERVER_ATTR(void)
{
	for (unsigned int i = 0; i < this->attrs.size(); i++)
	{
		delete this->attrs[i];
	}
}

unsigned char SERVER_ATTR::GetAttr(int x, int y)
{
	int new_x = x/25600;
	int new_y = y/25600;


	int local_x = (x/100) - (256*new_x);
	int local_y = (y/100) - (256*new_y);
	char* sq = this->attrs[new_y*this->x_size+new_x];
	if (sq)
	{
		sq += 5; // 5 pierwszych bajt�w to header
		int byt = 256*local_y + local_x;
		return *(unsigned char*)(sq + byt);
	}
	return 0xFF;
}
