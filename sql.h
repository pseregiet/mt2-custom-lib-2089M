/* ---------------------------------------------------------------------------
** 								By DoRaeMoN
**
** 							www.RoyalSword.org
** -------------------------------------------------------------------------*/

#ifndef MYSQL_H
#define MYSQL_H


#define mysql_fetch_row(a) ((char**(*)(int))0x082D7050)(a)
#define mysql_fetch_field(a) ((int(*)(int))0x082D93A0)(a)
#define mysql_store_result(a) ((int(*)(int))0x082D5750)(a)
#define mysql_thread_id(a) ((int(*)(int))0x082D98B0)(a)
#define mysql_real_query(a,b,c) ((int(*)(int, const char*, unsigned long))0x082D46A0)(a,b,c)

#include <string>
#include <vector>

#include "DB_MANAGER.h"
#include "misc.h"

typedef struct st_mysql_field {
  char *name;                 /* Name of column */
  char *org_name;             /* Original column name, if an alias */
  char *table;                /* Table of column if column was a field */
  char *org_table;            /* Org table name, if table was an alias */
  char *db;                   /* Database for table */
  char *catalog;	         /* Catalog for table */
  char *def;                  /* Default value (set by mysql_list_fields) */
  unsigned long length;       /* Width of column (create length) */
  unsigned long max_length;   /* Max width for selected set */
  unsigned int name_length;
  unsigned int org_name_length;
  unsigned int table_length;
  unsigned int org_table_length;
  unsigned int db_length;
  unsigned int catalog_length;
  unsigned int def_length;
  unsigned int flags;         /* Div flags */
  unsigned int decimals;      /* Number of decimals in field */
  unsigned int charsetnr;     /* Character set */
  int some_enum_that_i_do_not_care_about; /* Type of field. See mysql_com.h for types */
  void *extension;
} MYSQL_FIELD;


int MYSQL_do_query(const char* query, std::vector < std::string > * buf);
int MYSQL_do_query_2(const char* q, char* s);
void do_Q(const char*q);

#endif
