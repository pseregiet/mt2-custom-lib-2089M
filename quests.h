#ifndef __LIB_QUESTS_HPP
#define __LIB_QUESTS_HPP

#include "SCHAR.h"
#include "ITEM.h"
#include "QUEST_MANAGER.h"
#include "LUA.h"
#include "DUNGEON.h"
#include "DUNGEON_MANAGER.h"
#include "SCHAR_MANAGER.h"
#include "misc.h"
#include "ITEM_MANAGER.h"
#include "hook_tools.h"
#include "sql.h"
#include "DB_MANAGER.h"
#include "SECTREE_MANAGER.h"

#include "pets.h"

void HOOK__QUESTS(int offset);

#endif
