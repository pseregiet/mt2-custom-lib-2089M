#ifndef __LIB_LUA_HPP
#define __LIB_LUA_HPP

#pragma once
class LUA
{
public:
	int pushnumber(double a1);
	long double tonumber(int a1);
	char* tostring(int a1);
	int pushstring(const char* a1);
	char isNumber(int a1);
	void saqf(void);
	int gaqf(void);
	void pushbool(bool a1);
	void newtable(void);
	void rawset(int a1);
};

#endif


