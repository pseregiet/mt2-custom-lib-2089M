/*
Dynamic loaded library by Patryk Seregiet
(c) 2012-2014
for game revision : 2089

Please keep in mind that:
1. This source code is pretty much useless now. The original source code of metin2 has been leaked
So you don't need to code such lib unless you just want to do it.
2. This was never meant to be seen by anyone and I was 16-18 when I was coding it. I added
some comments in English recently for context sake, but other than that, I didn't change anything !
3. I've made a lot of different fixes and features for various mt2 server versions, this project
is propably the biggest one, although it might not have the biggest and hardest hacks I did.

See README for more info and LICENSE for the license. 
*/



//#include <vector>
#include <dlfcn.h>
#include <stdio.h>
#include <sys/mman.h>
//#include <stdlib.h>
//#include <string>
//#include <ctime>
//#include <stdarg.h>
//#include <math.h>


/*#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <net/if.h>
#include <arpa/inet.h>*/


#include "types.h"

/*#include "commands.h"
#include "pvp.h"
#include "fishing.h"
#include "small_hooks.h"
#include "quests.h"
#include "guild_build.h"
#include "server_attr_hooks.h"
#include "chat.h"
#include "character_hooks.h"
#include "gm_restrictions.h"*/
#include "ip_secure_main.h"

#if defined(RTLD_NEXT)
#define REAL_LIBC RTLD_NEXT
#else
#define REAL_LIBC ((void *) -1L)
#endif

char engine = 0;
void func1(void);

typedef FILE *(*o_fopen_ptr) (const char *fn, const char *mode);
o_fopen_ptr o_fopen = (o_fopen_ptr) dlsym(REAL_LIBC, "fopen");

extern "C" FILE *fopen(const char *fn, const char *mode)
{
	if(!engine)
	{
		engine = 1;
		func1(); // <--- Tutaj jest nasz 'main'
	}
	return (*o_fopen)(fn, mode);
}

void func1(void)
{
	HOOK_IP_MAIN();

	/*HOOK_COMMANDS(offset);
	HOOK_PVP();
	HOOK__FISHING();
	HOOK__SMALL_HOOKS();
	HOOK__QUESTS();
	HOOK__GUILD_BUILD();
	HOOK__SERVER_ATTR_HOOKS();
	HOOK__CHAT();
	HOOK__CHARACTER_HOOKS();
	HOOK__GM_RESTRICTIONS();

	SERVER_ATTR_MANAGER* sam = sam->instance();
	sam->Initialize();*/
}

