#ifndef __LIB_PVP_HPP
#define __LIB_PVP_HPP

#include "SCHAR.h"
#include "types.h"
#include "misc.h"
#include "hook_tools.h"

extern "C" void HOOK_PVP(int off);

#endif
