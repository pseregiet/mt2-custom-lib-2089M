#ifndef __LIB_SECTREE_HPP
#define __LIB_SECTREE_HPP

#include "types.h"

class ATTRIBUTE;
class ENTITY;

#pragma once
class SECTREE
{
public:

	int GetAttribute(int a1, int a2);
	void RemoveEntity(ENTITY* a1);
	ATTRIBUTE* GetAttributeClass(void);
};

#endif


