﻿
#include "commands.h"

void COMMAND__STUN(SCHAR* a1, char* a2)
{
	SCHAR_MANAGER* x = SCHAR_MANAGER_SINGLETON;

	char xx[5];

	SCHAR* pc = x->FindPC(a2);
	if (pc)
	{
		
		pc->AddAffect(210, 0, 0, 6, 30, 0, 1, 0);
		a1->ChatPacket(3, "OK!");
		return;
	}
	a1->ChatPacket(3, "NIMA");
}
void COMMAND__PERM_STUN(SCHAR* a1, char* a2)
{
	SCHAR_MANAGER* x = SCHAR_MANAGER_SINGLETON;

	char xx[5];

	SCHAR* v1 = x->FindPC(a2);
	if (v1)
	{
		if (!((int(*)(SCHAR*,int))0x8080FB0)(v1, 6)) //SCHAR_IsAffect
		{
			v1->AddAffect(1005, 0, 0, 6, 99999, 0, 1, 0);
			a1->ChatPacket(3, "OK!");
			return;
		}
		v1->RemoveAffect(1005);
		a1->ChatPacket(3, "OK!");
		return;
	}
	a1->ChatPacket(3, "NIMA");
}

void COMMAND__PVP_SET_TIME(SCHAR* a1)
{
	int curr_time = get_global_time();
	a1->pvp_time = curr_time+10;
}

void COMMAND__PVP_ACCEPT(SCHAR* a1, char* a2)
{
	SCHAR_MANAGER* x = SCHAR_MANAGER_SINGLETON;

	char xx[15];

	int vid = atoi(a2);
	SCHAR* wyzywajacy = x->Find(vid);
	if (wyzywajacy)
	{
		if (a1->pvp_agree_time < get_global_time() )
		{
			return;
		}
		SCHAR* a3 = x->Find(wyzywajacy->vid);
		if (!a3)
		{
			return;
		}

		a1->pvp_time = get_global_time() + 10;
		char q[15];
		sprintf(q, "pvp_agree %d", wyzywajacy->vid);
		a1->ChatPacket(5, q);
		a3->ChatPacket(5, "pvp_agree %d", 1);
		a1->pvp_opponent_vid = wyzywajacy->vid;
	}
}

void COMMAND__TOGGLE_SHOW_HP(SCHAR* a1)
{
	char xx[10];
	if (a1->bShowStat)
	{
		a1->bShowStat = 0;
		a1->ChatPacket(1, locale_find("shpoff")); // Włączyłeś pokazywanie życia wroga -- shpoff
	}
	else
	{
		a1->bShowStat = 1;
		a1->ChatPacket(1, locale_find("shon")); // Wyłączyłeś pokazywanie życia wroga -- shon
	}
}

void COMMAND__TOGLE_TEST_SERVER(SCHAR* a1)
{
	char ts = TEST_SERVER;
	TEST_SERVER = 1 - ts;
	a1->ChatPacket(1, "Test server = %d", TEST_SERVER); // Test server = %d
}

void COMMAND__DANCE6(SCHAR* a1)
{
	a1->ActEmotion("dance6"); // dance6
}

void COMMAND__TEST_QUERY(SCHAR* a1)
{
	a1->ChatPacket(3, "rev = 8291, global_time_now = %d", get_global_time());
}

void COMMAND__PET_SET_EXP(SCHAR* a1)
{
	a1->b_petexp = a1->b_petexp + 1;
	if (a1->b_petexp == 6)
	{
		a1->b_petexp = 0;
	}
	if (!a1->b_petexp)
	{
		a1->ChatPacket(1, locale_find("pet_exp_of"));//Dzielenie doświadczenia ze zwierzakiem zostało wyłączone -- pet_exp_of
	}
	else
	{
		a1->ChatPacket(1, locale_find("pet_exp_set"), a1->b_petexp);//Dzielenie doświadczenia ze zwierzakiem ustawione na %d0%% -- pet_exp_set
	}
}
void COMMAND__UNEQUIP_COSTUME(SCHAR* player, char* arg)
{
	int a = atoi(arg);
	if (a > 1 && a < 0)
	{
		return;
	}
	unequip_costume(player, a, 1);
}
struct command_struct
{
	char command[20];
	void* call;
	char gm_level;
};

command_struct noarg_table[NOARG_COMMAND] = 
{
	{"pvp_set_time",	(void*)&COMMAND__PVP_SET_TIME,		5}, //pvp_set_time
	{"shp",				(void*)&COMMAND__TOGGLE_SHOW_HP,	0}, //shp
	{"tts",				(void*)&COMMAND__TOGLE_TEST_SERVER, 5}, //tts
	{"damce6",			(void*)&COMMAND__DANCE6,			5}, //damce6
	{"qtest",			(void*)&COMMAND__TEST_QUERY,		5}, //qtest
	{"pexp",			(void*)&COMMAND__PET_SET_EXP,		0}, //pexp
};

command_struct arg_table[ARG_COMMAND] = 
{
	{"stun",			(void*)&COMMAND__STUN,				1}, //stun
	{"pstun",			(void*)&COMMAND__PERM_STUN,			1}, //pstun
	{"pvp_a",			(void*)&COMMAND__PVP_ACCEPT,		0}, //pvp_a
	{"uqc",				(void*)&COMMAND__UNEQUIP_COSTUME,	0}, //uqc
};


extern "C" int command_hook_c(char* command, SCHAR * player)
{
	char com[100];
	char arg[100];
	std::string x(command);
	int f = x.find("/");
	if (f != std::string::npos)
	{
		strncpy(com, command, f);
		com[f] = 0;
		strcpy(arg, command+f+1);

		for (int i = 0; i < ARG_COMMAND; i++)
		{
			if (strcmp(arg_table[i].command, com) == 0)
			{
				if (player->gm_level >= arg_table[i].gm_level)
				{
					( ( void ( * ) ( SCHAR*, char* ) ) arg_table[i].call )(player, arg);
					return 1;
				}
			}
		}	
	}
	else
	{

		for (int i = 0; i < NOARG_COMMAND; i++)
		{
			if (strcmp(noarg_table[i].command, command) == 0)
			{
				if (player->gm_level >= noarg_table[i].gm_level)
				{
					( ( void ( * ) ( SCHAR* ) ) noarg_table[i].call )(player);
					return 1;
				}
			}
		}	
	}
	return 0;
}

extern "C" void command_hook_asm()
{
	asm(
	".intel_syntax noprefix\n"
	".globl command_hook_c\n"
	"add esp, 4\n" // usuwa adres powrotu
	"pushad\n"
	"push edi\n" // char pointer
	"push edx\n" // command
	"call command_hook_c\n"
	"add esp, 8\n" // usuwa argumenty ze stosu
	"test eax, eax\n" // sprawdza co zwrocila funkcja
	"jnz label_fail\n"
	"popad\n"
	"mov eax, 0x80C2B80\n"
	"call eax\n"
	"xor eax, eax\n"
	"push 0x080C2D6D\n"
	"ret\n"
	"label_fail:\n"
		"popad\n"
		"mov eax, 0x080C2D35\n" // skok na koniec funkcji
		"jmp eax\n"
	);
}

void HOOK_COMMANDS(int offset)
{
	CallPatch((int)&command_hook_asm+3, IPS(0x080C2D68+offset), 0); // 2B5 / 294 / 0x80C2D68
	//printf("HOOK_COMMANDS OK ! 0ffset = 0x%X\n", offset);
}
