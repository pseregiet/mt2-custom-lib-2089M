#ifndef __LIB_SMALL_HOOKS_HPP
#define __LIB_SMALL_HOOKS_HPP

#include "types.h"
#include "DB_MANAGER.h"
#include "hook_tools.h"
#include "ITEM.h"
#include "SCHAR.h"
#include "misc.h"
#include <string.h>
#include <stdio.h>




void HOOK__SMALL_HOOKS(int offset);

#endif
