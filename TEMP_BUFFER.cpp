#include "TEMP_BUFFER.h"


TEMP_BUFFER::TEMP_BUFFER(int size, bool forceDelete)
{
	((void(*)(TEMP_BUFFER*, int, bool))0x08062F90)(this, size, forceDelete);
}


TEMP_BUFFER::~TEMP_BUFFER(void)
{
	((void(*)(TEMP_BUFFER*))0x08062F30)(this);
}
const void * TEMP_BUFFER::read_peek(void)
{
	return ((void*(*)(TEMP_BUFFER*))0x08062F00)(this);
}
void TEMP_BUFFER::write(const void * a1, int a2)
{
	((void(*)(TEMP_BUFFER*, const void*, int))0x08062EC0)(this,a1,a2);
}
int TEMP_BUFFER::size(void)
{
	return ((int(*)(TEMP_BUFFER*))0x08062E90)(this);
}
void TEMP_BUFFER::reset(void)
{
	((void(*)(TEMP_BUFFER*))0x08062E60)(this);
}
