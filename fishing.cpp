#include "fishing.h"


std::vector <fishing> fishing_vector;

extern "C" int RollFish(SCHAR* a1)
{
	ITEM* wedka = a1->GetWear(4);
	if (!wedka) return 0;
	int skill =wedka->ItemProto->ID - 27399;// SCHAR_GetSkillLevel(a1, 134);
	int map = a1->map_index;
	if (map == 1 || map == 21 || map == 41)
	{
		map = 1;
	}
	else if (map == 3 || map == 23 || map == 43)
	{
		map = 2;
	}
	int size = fishing_vector.size();
	
	int start = 0;
	int end = 0;
	for (int i = 0;i<size;i++)
	{
		fishing s = fishing_vector[i];
		if (!start)
		{
			if (s.map_index == map  || s.map_index == 0 && s.level <= skill)
			{
				start = i+1;
				continue;
			}
		}
		else
		{
			if (s.map_index != map && s.map_index != 0 || s.level > skill)
			{
				end = i+1;
				break;
			}
		}
	}
	if (!end)
	{
		end = size;
	}
	return RandomNumber(start-1, end-1);
}

extern "C" int TryCatchFish(SCHAR* a1, int i, DWORD tu_id_ryby_wpakuj)
{
	ITEM* item2 = a1->GetWear(4);
	if (item2)
	{
		int id = item2->ItemProto->ID;
		int chance_plus = (id - 27400)*5; // poziom wedki * 5. Wedka +1 nie dodaje nic
		int los = RandomNumber(1, 1000);
		if (los<= fishing_vector[i].chance + chance_plus)
		{
			*(int*)tu_id_ryby_wpakuj = fishing_vector[i].id_live;
			
			/*add achievement here */
			// tutaj wpakowa? sprawdzanie osi?gni?? dla ryb
			//checkPartAchievement(5, 0, a1);
			
			return 0;
		}
		return -3;
	}
	return -2;
}
extern "C" void fishing_take_hook_asm3(void)
{
	asm(".intel_syntax noprefix\n"
		".globl TryCatchFish\n"
		"add esp, 4\n"
		"lea eax, [ebp-0x34]\n"
		"push eax\n"
		"push edx\n"
		"mov eax, [ebp-0x7C]\n"
		"push eax\n"
		"call TryCatchFish\n"
		"add esp, 12\n"
		"push 0x081012DC\n"
		"ret\n"
	);
}

extern "C" void FishLength_asm(void) 
{
	asm(".intel_syntax noprefix\n"
		".globl FishLength\n"
		"add esp, 4\n"
		"push eax\n"
		"mov eax, [ebp+12]\n"
		"push eax\n"
		"call FishLength\n"
		"add esp, 8\n"
		"push 0x08101379\n"
		"ret\n"
		);
}

extern "C" int FishLength(SCHAR* a1, int i)
{
	int rand = RandomNumber(fishing_vector[i].min_len, fishing_vector[i].max_len);
	//if(rand > 5000) // Osi?gni?cie za d?ugo?? ryby
	//	checkAndShowCompleteAchievement(a1, AchievementList[0]);
	return rand;
}
extern "C" void GrillFish(SCHAR* a1, ITEM* itemx)
{
	item_proto* item_base = itemx->ItemProto;;
	if (!item_base)
	{
		return;
	}
	int id = item_base->ID;
	for (unsigned int i = 0; i < fishing_vector.size(); i++)
	{
		if (fishing_vector[i].id_dead == id && fishing_vector[i].id_grill)
		{
			int count = itemx->GetCount();
			a1->AutoGiveItem(fishing_vector[i].id_grill, count, -1, 1);
			itemx->SetCount(0);
			break;
		}
	}
}
extern "C" void receive_item_hook_asm(void) // grill_fish
{
	asm(".intel_syntax noprefix\n"
		".globl GrillFish\n"
		"add esp, 4\n"
		"push ebx\n"
		"mov eax, [ebp+8]\n"
		"push eax\n"
		"call GrillFish\n"
		"add esp, 8\n"
		"push 0x80940E9\n"
		"ret\n"
	);
}
extern "C" void UseFish(SCHAR* a1, ITEM* itemx)
{
	item_proto* item_base = itemx->ItemProto;
	if (!item_base)
	{
		return;
	}
	int id = item_base->ID;
	for (unsigned int i = 0; i < fishing_vector.size(); i++)
	{
		if (fishing_vector[i].id_live == id && fishing_vector[i].id_dead)
		{
			int rand = RandomNumber(1, 100);
			if (rand <= 10)
			{
				a1->AutoGiveItem(27987, 1, -1, 1);
			}
			else
			{
				if (rand <= 21)
				{
					a1->AutoGiveItem(27799, 1, -1, 1);
				}
				else
				{
					a1->AutoGiveItem(fishing_vector[i].id_dead, 1, -1, 1);
				}
			}
			int count = itemx->GetCount();
			itemx->SetCount(count-1);
			break;
		}
	}
}

extern "C" void fishing_event_hook_c3(int i, int p, SCHAR* s)
{
	*(char*)p = 0x59;
	*(char*)(p+1) = 5;
	*(int*)(p+2) = fishing_vector[i].id_live;
	s->lpDesc->Packet((const void*)p, 7);
}
extern "C" void fishing_event_hook_asm3(void)
{
	asm(".intel_syntax noprefix\n"
		".globl fishing_event_hook_c3\n"
		"add esp, 4\n"
		"push ebx\n"
		"lea eax, [ebp-0x17]\n"
		"push eax\n"
		"mov eax, [edi+12]\n"
		"push eax\n"
		"call fishing_event_hook_c3\n"
		"add esp, 12\n"
		"push 0x08101D87\n"
		"ret\n"
	);
}

extern "C" void fishing_event_hook_c(SCHAR* player)
{
	player->fishing_catch = 1;
	char x= RandomNumber(2, 4);
	player->fishing_set_count = x;
	
	for (int i = 0; i<x;i++)
	{
		player->CreateFlyOnlyForMe(RandomNumber(7,13), player);
		//player->CreateFly(RandomNumber(7,13), player);
	}
}
extern "C" void fishing_event_hook_asm(void)
{
	asm(".intel_syntax noprefix\n"
		".globl fishing_hook_c\n"
		"add esp, 4\n"
		"call fishing_event_hook_c\n"
		"push 0x08101DFF\n"
		"ret\n"
	);
}
extern "C" char fishing_event_hook_c2(SCHAR* a1)
{
	char cur = a1->fisging_cur_cound;
	a1->fisging_cur_cound = 0;
	a1->fishing_catch = 0;
	if (cur == a1->fishing_set_count)
	{
		a1->FishTake();
		return 1;
	}
	return 0;
}
extern "C" void fishing_event_hook_asm2(void)
{
	asm(".intel_syntax noprefix\n"
		".globl fishing_event_hook_c2\n"
		"add esp, 4\n"
		"mov eax, [edi+4]\n"
		"test eax, eax\n"
		"jnz fish_time_up\n"
		"push 0x08101D59\n"
		"ret\n"

		"fish_time_up:\n"

		"mov [esp], ebx\n"
		"call fishing_event_hook_c2\n"
		"test eax, eax\n"
		"jnz fish_succes\n"
		"push 0x08101D93\n"
		"ret\n"

		"fish_succes:\n"

		"push 0x08101DCF\n"
		"ret\n"
	);
}

extern "C" void fish_add_count(SCHAR* a1)
{
	if (a1->fisging_cur_cound <= 15 && a1->fishing_catch)
	{
		a1->fisging_cur_cound++;
	}
}
void InitializeFishing(void)
{
	//fishing_vector = new std::vector <fishing>;
	fishing_vector.clear();
	//printf("Initializuje fishing_new.txt\n");
	FILE* pFile;
	pFile = fopen("locale/germany/fishing_new.txt", "r"); //locale/germany/fishing_new.txt
	if (pFile == NULL)
	{
		//printf("Blad podczas otwierania fishing_new.txt !!\n");
		return;
	}
	char line[100];
	char* pch;
	while (!feof(pFile))
	{
		fgets(line, 100, pFile);
		pch = strtok (line,"	");
		int co = 0;
		fishing s;
		while (pch != NULL)
		{
			int x = (int)&s;
			*(int*)(x+4*co) = atoi(pch);
			pch = strtok (NULL, "	");
			co++;
		}
		fishing_vector.push_back(s);
	}
	fclose(pFile);
	//printf("Poprawnie zainicjowano fishing_new.txt !\n");
}

void HOOK__FISHING(int off)
{
	//Do lowienia :#

	CallPatch((int)&RollFish,					IPS(off+0x08101D6B), 0); // 
	CallPatch((int)&fishing_event_hook_asm3+3,	IPS(off+0x08101DFF), 0); // 
	CallPatch((int)&fishing_take_hook_asm3+3,	IPS(off+0x081012D7), 0); //
	CallPatch((int)&UseFish,					IPS(off+0x080985E9), 0); // 
	CallPatch((int)&receive_item_hook_asm+3,	IPS(off+0x08094299), 0); // 
	CallPatch((int)&FishLength_asm+3,			IPS(off+0x08101374), 0); // 
	CallPatch((int)&fishing_event_hook_asm+3,	IPS(off+0x08101D7E), 0); // 
	CallPatch((int)&fishing_event_hook_asm2+3,	IPS(off+0x08101D52), 0); // 
	CallPatch((int)&fish_add_count,				IPS(off+0x08070CC9), 0);

	fill_nop(									IPS(off+0x081000C5), 6);

	//printf("HOOK_FISHING OK\n");

	InitializeFishing();
}
