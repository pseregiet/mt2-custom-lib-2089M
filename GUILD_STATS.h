#ifndef __LIB_GUILD_STATS_HPP
#define __LIB_GUILD_STATS_HPP

#include <string.h>
#include <stdlib.h>

#include "SCHAR.h"

class SCHAR;

/*Basic KIll/Death stats window for guild's wars */

#pragma once
class GUILD_STATS
{
public:
	int players[88][3];

	void PlayerJoin(SCHAR* a1);
	void PlayerKill(SCHAR* a1);
	void PlayerDead(SCHAR* a1);
	void SortStats(int* a1);

	GUILD_STATS(void);

};

#endif
