#ifndef __LIB_ITEM_PROTO_HPP
#define __LIB_ITEM_PROTO_HPP

#pragma pack(1)
struct ITEM_VALUE
{
	char LimitType;
	int LimitValue;
};
#pragma pack()

#pragma pack(1)
struct ITEM_BONUS
{
	char id;
	short val;
};
#pragma pack()

#pragma pack(1)
struct item_proto
{
	int ID; // 0 - 3
	char unkn1[50]; // 4-53
	char Type; // 54
	char SubType; // daje na probe 55
	char weight; // 56
	int size; // 57 - 60
	char unkn4[5]; // 61 - 65
	char WearFlag; // 66
	char unkn2[7]; // 67 - 73
	int Gold; // 74 - 77
	int ShopBuyPrice; // 78 - 81
	ITEM_VALUE item_limit[2];
	ITEM_VALUE item_apply[3];
	int Value[6];
};
#pragma pack()


#endif
