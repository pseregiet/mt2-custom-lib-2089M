#include "server_attr_hooks.h"

extern "C" void LoadAttribute_c(int map_index, ATTRIBUTE* Cattribute, DWORD* a4)
{
	((void(*)(ATTRIBUTE*, DWORD*, DWORD, DWORD))0x8337C80)(Cattribute, a4, 128, 128);
	// Cattribute::Catribute
	Cattribute->map_index = map_index;
}
void LoadAttribute_asm(void) //0x081CD52F
{
	asm(".intel_syntax noprefix\n"
		".globl LoadAttribute_c\n"

		"add esp, 4\n"

		"push eax\n" // uint*
		"push ebx\n" // Cattribute*

		"mov eax, [ebp - 0x50]\n" // map_index int
		"push eax\n"
		//"mov eax, [ebp + 16]\n" // char* map/server_attr
		//"push eax\n"

		"call LoadAttribute_c\n"

		"add esp, 12\n"
		"push 0x081CD534\n"
		"ret\n"
		);
}

extern "C" int SECTREE__GetAttribute(SECTREE* a1, DWORD x, DWORD y)
{
	SERVER_ATTR* atr = 0;
	SERVER_ATTR_MANAGER* sam = sam->instance();
	int map_index = a1->GetAttributeClass()->map_index;
	for (DWORD i = 0;i<sam->maps.size();i++)
	{
		if (sam->maps[i] == map_index)
		{
			atr = sam->server_attr_maps[i];
		}
	}
	if (atr)
	{
		return atr->GetAttr(x,y);
	}
	else
	{
		return a1->GetAttribute(x,y);
	}
}

void HOOK__SERVER_ATTR_HOOKS(int off)
{
	mprotect((const void*)0x081CD507, 1, 7);
	*(DWORD*)0x081CD507 = 36;

	CallPatch((int)&LoadAttribute_asm+3,		IPS(off+0x081CD52F), 0);

	int addr[7] = {0x08070D32, 0x080745FA, 0x080A7105, 
					0x080A83B4, 0x080D0DE0, 0x081CA374,
					0x081CA3B8 };
	for (int i = 0; i<7;i++)
	{
		CallPatch((int)&SECTREE__GetAttribute,	IPS(off+addr[i]), 0);
	}

	//printf("HOOK__SERVER_ATTR_HOOKS OK\n");
}