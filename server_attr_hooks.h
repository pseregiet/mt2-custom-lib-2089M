#ifndef __LIB_SERVER_ATTR_HOOKS_HPP
#define __LIB_SERVER_ATTR_HOOKS_HPP

#include "types.h"
#include "hook_tools.h"
#include "misc.h"
#include "SERVER_ATTR_MANAGER.h"
#include "SERVER_ATTR.h"
#include "SCHAR.h"
#include "ATTRIBUTE.h"
#include "SECTREE.h"

void HOOK__SERVER_ATTR_HOOKS(int off);

#endif
