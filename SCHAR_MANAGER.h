#ifndef __LIB_SCHAR_MANAGER
#define __LIB_SCHAR_MANAGER

#include "types.h"
#include "SCHAR.h"
#include "GameSingleton.h"

#define SCHAR_MANAGER_SINGLETON (SCHAR_MANAGER*)*(DWORD*)(0x84C485C)

#pragma once
class SCHAR_MANAGER : public GameSingleton<SCHAR_MANAGER, 0x84C485C>
{
public:

	SCHAR* FindPC(const char* a1);
	SCHAR* Find(int vid);
	void DestroyCharacter(SCHAR* a1);
	SCHAR* SpawnMob(DWORD vnum, long map_index, long x, long y, long a5, bool a6, int a7, bool a8);
};

#endif


