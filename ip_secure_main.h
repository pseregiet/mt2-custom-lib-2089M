#ifndef __TENMA_IP_SECURE_MAIN_HPP
#define __TENMA_IP_SECURE_MAIN_HPP

/*
Zabezpieczenie ca?ej biblioteki na jedno IP.
Funkcja kalkuluje offset na podstawie IP
Wi?c, je?eli IP jest inne, wszystkie adresy b?d? z?e
I mamy gwarantowany crash. Nic super skomplikowanego
Ka?dy kto zna chocia? troche RE i mia?by ochote
To omin?? by?by w stanie to zrobi?, ale jest to
jakie? dodatkowe zabezpieczenie na lamer�w (Patryk 5.5.2013)
*/

#define fprint__ 0x804C16C
#include "hook_tools.h"
#include "types.h"

#include "commands.h"
#include "costumes.h"
#include "character_hooks.h"
#include "pets.h"
#include "quests.h"
#include "small_hooks.h"
#include "fishing.h"
#include "pvp.h"
#include "chat.h"
#include "shop.h"
//#include "server_attr_hooks.h"

int HOOK_IP_MAIN(void);


#endif
