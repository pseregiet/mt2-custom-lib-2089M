#include "shop.h"

extern "C" ITEM* Shop_hook_c(int shop, SCHAR* a1, int id, ITEM* item_p)
{
	if (!item_p)
	{
		return 0;
	}
	if (id == 70104)
	{
		int socket;
		socket = 101; // Tutaj dac losowanie
		item_p->SetSocket(0, socket);
	}
	int pkShop = *(int*)((int)a1 + 3296);
	if (pkShop)
	{
		int race = *(int*)(pkShop + 4); // Race npc ze sklepem
		if (race == 9010)
		{
			// Dodać sprawdzanie event flag

			ITEM_MANAGER* IM = ITEM_MANAGER_SINGLETON;
			item_proto* itt = IM->GetTable(id);
			if (itt)
			{
				if (a1->GetEmptyInventory(itt->size) == -1)
				{
					IM->RemoveItem(item_p, 0);
					a1->ChatPacket(1,"Nie masz miejsca w ekwipunku");
					return 0;
				}
			}
			char q[1024];
			std::vector <std::string> rect;
			sprintf(q, "SELECT `coins` FROM account.account WHERE `id` = %d", a1->lpDesc->m_accountTable.id);
			if (TEST_SERVER)
			{
				a1->ChatPacket(3, q);
			}
			MYSQL_do_query(q, &rect);
			int your_sm = 0;
			if (rect.size())
			{
				your_sm = atoi(rect[0].c_str());
			}
			sprintf(q, "SELECT `price` FROM player.item_proto_shop WHERE `vnum` = %d", id);
			rect.clear();
			MYSQL_do_query(q, &rect);
			int price = 0;
			if (rect.size())
			{
				price = atoi(rect[0].c_str());
			}
			if (price > 0 && your_sm >= price)
			{
				sprintf(q, "UPDATE account.account set coins = coins - %d where `id` = %d", price, a1->lpDesc->m_accountTable.id);
				MYSQL_do_query(q, 0);
				sprintf(q, "INSERT INTO log.log_is VALUES(null, %d, %d, %d, %d, NOW(), ' ', 'IS')", item_p->ItemProto->ID, *(int*)((int)item_p + 80), a1->lpDesc->m_accountTable.id, a1->player_id);

				a1->ChatPacket(5, "setc %d", your_sm-price);
				return item_p;
			}
			if (TEST_SERVER)
			{
				a1->ChatPacket(3, "price = %d, your_sm = %d", price, your_sm);
			}
			IM->RemoveItem(item_p, 0);
			a1->ChatPacket(1, locale_find("nism%d%dbuza"), your_sm, price);
			return 0;
		}
	}
	return item_p;
}

extern "C" void shop_hook_asm()
{
	asm(".intel_syntax noprefix\n"
		".globl Shop_hook_c\n"
		"push eax\n"
		"push ebx\n"
		"mov eax, [ebp-0x230]\n"
		"push eax\n"
		"mov eax, [ebp+8]\n"
		"push eax\n"
		"call Shop_hook_c\n"
		"add esp, 16\n"
		"test eax, eax\n"
		"jz shop_hook_fail\n"
		"mov [ebp-0x258], eax\n"
		"ret\n"

		"shop_hook_fail:\n"
		
		"mov eax, 4\n"
		"add esp, 4\n"
		"push 0x81D4A08\n"
		"ret\n"
	);
}

void HOOK__SHOP(int offset)
{
	CallPatch((int)&shop_hook_asm+3, IPS(offset+0x081D54DF), 1);
}
