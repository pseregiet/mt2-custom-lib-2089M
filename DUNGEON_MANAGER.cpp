#include "DUNGEON_MANAGER.h"


DUNGEON* DUNGEON_MANAGER::Create(int index)
{
	return ((DUNGEON*(*)(DUNGEON_MANAGER*,int))0x080F8B80)(this,index);
}


DUNGEON* DUNGEON_MANAGER::Find(long index)
{
	return ((DUNGEON*(*)(DUNGEON_MANAGER*,long))0x080F42D0)(this,index);
}
