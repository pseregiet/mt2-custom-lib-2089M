#include "pets.h"


int pet_exp_table[31] = {
								875000,
								1000000,
								1125000,
								1250000,
								1875000,
								2000000,
								2250000,
								2500000,
								3125000,
								3625000,
								4000000,
								4500000,
								4875000,
								5375000,
								6000000,
								6500000,
								7250000,
								7875000,
								8750000,
								9625000,
								10625000,
								11625000,
								16250000,
								12750000,
								14125000,
								15125000,
								17125000,
								18750000,
								20625000,
								29500000,
								32500000,
};

/*Function for naming your pet. There was something wrong in the original
function, I copy pasted the dissasembly and fixed it. I don't remember anymore
what exactly was wrong there. This function is called from a quest (look quests.cpp)
*/

void pet_name_asm(SCHAR* pet, char* name, int len)
{
	asm(".intel_syntax noprefix\n"
		"push edi\n"
		"push esi\n"
		"push ebx\n"
		"sub esp, 40\n"
		"mov eax, [ebp+8]\n"
		"test eax, eax\n"
		"jnz pet_detected\n"
		"jmp end_of_name_asm\n"

		"pet_detected:\n"

		"add eax, 260\n"
		"mov [esp], eax\n"
		"mov ebx, [ebp+12]\n"
		"mov [esp+4], ebx\n"
		"mov ebx, [ebp+16]\n"
		"mov [esp+8], ebx\n"
		"mov eax, 0x804D16C\n"
		"call eax\n"
		"jmp end_of_name_asm\n"

		"end_of_name_asm:\n"

		"add esp, 40\n"
		"pop ebx\n"
		"pop esi\n"
		"pop edi\n"
		"pop ebp\n"
		"ret\n"
	);
}

int distance(SCHAR * char1, SCHAR * char2) 
{
	return sqrt(pow(char2->x - char1->x, 2) + pow(char2->y - char1->y, 2))/100;
}

bool raceisPet(int race)
{
	if (race >= 34001 && race <= 34009)
	{
		return true;
	}
	return false;
}


/*I hook the function that creates NPCs and trick it into thinking that PETs are HORSES
Why ? Because a HORSE follows his RIDER (you), instead of writing my own AI code for pets
I just make them behave like HORSES*/

extern "C" int PetID_hook_c(SCHAR* a1)
{
	int id = a1->GetRaceNum();
	if (!raceisPet(id))
	{
		return id;
	}
	return 0;
}

extern "C" void PetID_hook_asm(void)
{
	asm(".intel_syntax noprefix\n"
		".globl PetID_hook_c\n"
		"add esp, 4\n"
		"call PetID_hook_c\n"
		"test eax, eax\n"
		"jz is_pet\n"
		"push 0x0807D00D\n"
		"ret\n"

		"is_pet:\n"

		"push 0x807D056\n"
		"ret\n"
	);
}

/*Admins have a command /purge which deletes all monsters and NPCs in certain
radius. We need to hook this function and clear few things. I also added
a chat messagge letting you know why did you PET dissapear */
extern "C" void Purge_hook_c(SCHAR_MANAGER* CM, SCHAR* a1)
{
	int num = a1->GetRaceNum();
	if (raceisPet(num))
	{
		a1->rider->pet = 0;
		SCHAR* pc = a1->rider;
		a1->rider = 0;

		pc->SetQuestFlag("pet.is_summon", 0);
		a1->ChatPacket(1, locale_find("pedisga")); //Your pet has vanished, probably because of GM
	}
	CM->DestroyCharacter(a1);
}

extern "C" void purge_hook_asm(void)
{
	asm(".intel_syntax noprefix\n"
		".globl Purge_hook_c\n"
		"add esp, 4\n"
		"call Purge_hook_c\n"
		"push 0x080D1AF2\n"
		"ret\n"
	);
}

/*This function is just sort of a safety net. Could be skipped
It looks for a list of all characters on the server and checks
if it really exist. Prevents deleting bad pointers*/

void DestroyIfExist(SCHAR* a1)
{
	SCHAR_MANAGER* CM = SCHAR_MANAGER_SINGLETON;
	if (CM->Find(a1->vid))
	{
		CM->DestroyCharacter(a1);
	}
}

void PET_Destroy(SCHAR* a1)
{
	if (a1->rider)
	{
		a1->rider = 0;
	}
	DestroyIfExist(a1);
}

/*The exp system... if your PET is close enough to you and you have
enabled exp sharing, every piece of experience you get from killed monsters
will be split between you and your PET.*/

extern "C" int pet_exp_system_c(SCHAR * player, SCHAR * mob, int exp) // 08086D5E  8nop
{
	if (player->pet && player->pet->level < 20 && player->b_petexp && distance(player, player->pet) <= 30)
	{
		std::string qfname = "pet.exp";
		std::string qfname2 = "pet.level";
		mob->CreateFly(1, player->pet);
		int exp_p = player->GetQuestFlag(qfname)+exp/10*player->b_petexp;
		player->SetQuestFlag(qfname, exp_p);
		player->ChatPacket(5, "pet_e %d", exp_p);
		if (player->GetQuestFlag(qfname) >= pet_exp_table[player->pet->level-1])
		{
			player->SetQuestFlag(qfname, 0);
			player->pet->level++;
			player->SetQuestFlag(qfname2, player->pet->level);
			player->ChatPacket(1, locale_find("pelvup"), player->pet->level); //Your pet has level'd up ! (lv %d)
			player->ChatPacket(5, "pet_l %d", player->pet->level);
		}
		return exp/10*(10-player->b_petexp);
	}
	return exp;
}

extern "C" void pet_exp_system_asm()
{
	asm(".intel_syntax noprefix\n"
		".global pet_exp_system_c\n"
		"add esp, 4\n"
		"push eax\n"
		"mov eax, [ebp-0x30]\n"
		"push eax\n"
		"push edi\n"
		"call pet_exp_system_c\n"
		"add esp, 12\n"
		"mov dword ptr [esp+0x10], 0\n"
		"mov ebx, 0x08086D66\n"
		"jmp ebx\n"
	);
}


void HOOK__PETS(int offset)
{
	CallPatch((int)&purge_hook_asm+3, IPS(offset+0x080D1AED), 0);
	CallPatch((int)&PetID_hook_asm+3, IPS(offset+0x0807D008), 0);
	CallPatch((int)&pet_exp_system_asm+3, IPS(offset+0x08086D5E), 3);
}
