#ifndef __LIB_MISC_HPP
#define __LIB_MISC_HPP

//#define DED

#include "types.h"

#define TEST_SERVER *(char*)(0x84C5DAC)
#define PASSES_PER_SEC *(int*)(0x847741C)
#define EMPIRE_WHISPER 0x08477458
#define APPLY_INFO 0x08347FA0
#define EXP_TABLE_COMMON 0x083477A0
#define sys_err 0x08316140
#define CHANNEL *(char*)(0x084C5D8C)
#define START_POSTION 0x847A380
#define CMD_INFO 0x08476240


#define SendNoticeMap(a,b,c) ((void(*)(const char*,int,bool))0x80CC910)(a,b,c)
#define get_dword_time() ((DWORD(*)(void))0x08316470)()
#define get_global_time() ((DWORD(*)(void))0x081DE6E0)()
#define RandomNumber(a,b) ((int(*)(int,int))0x08316650)(a,b)
#define event_time(a) ((int(*)(Event*))0x080FBD20)(a)
#define event_cancel(a) ((void(*)(Event**))0x080FC670)(a)

#define locale_find(arg) ((char*(*)(const char*))0x814A390)(arg)

#ifdef DED
	#define IPS(a) (a-0x2B5) //249
#else
	#define IPS(a) (a-0x294)
#endif

#endif
