#ifndef __LIB_SHOP_HPP
#define __LIB_SHOP_HPP

#include <cstdlib>
#include "hook_tools.h"
#include "misc.h"
#include "types.h"
#include "ITEM.h"
#include "ITEM_MANAGER.h"
#include "SCHAR.h"
#include "sql.h"


void HOOK__SHOP(int offset);
#endif
