#include "SECTREE.h"

void SECTREE::RemoveEntity(ENTITY* a1)
{
	((void(*)(SECTREE*,ENTITY*))0x081CA3C0)(this,a1);
}

int SECTREE::GetAttribute(int a1, int a2)
{
	return ((int(*)(SECTREE*,int,int))0x81CA280)(this,a1,a2);
}

ATTRIBUTE* SECTREE::GetAttributeClass(void)
{
	return (ATTRIBUTE*)*(DWORD*)((DWORD)this + 44);
}
