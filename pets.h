#ifndef __LIB_PETS_HPP
#define __LIB_PETS_HPP

#include "SCHAR.h"
#include "hook_tools.h"
#include "SCHAR_MANAGER.h"
#include "types.h"
#include "misc.h"

#include "math.h"

void HOOK__PETS(int offset);

void PET_Destroy(SCHAR* a1);
void pet_name_asm(SCHAR* pet, char* name, int len);
#endif

