#ifndef __LIB_CHARACTER_HOOKS_HPP
#define __LIB_CHARACTER_HOOKS_HPP

#include "SCHAR.h"
#include "types.h"
#include "SECTREE_MANAGER.h"
#include "hook_tools.h"
#include "SCHAR_MANAGER.h"

void HOOK__CHARACTER_HOOKS(int offset);

#endif
