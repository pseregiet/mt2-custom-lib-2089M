
#include "character_hooks.h"

int mount_table[95][2] = 
{
	{71164, 20110},
	{52001, 20201},
	{52002, 20201},
	{52003, 20201},
	
	{52004, 20201},
	{52005, 20201},
	{52006, 20205},
	{52007, 20205},
	
	{52008, 20205},
	{52009, 20205},
	{52010, 20205},
	{52011, 20209},
	
	{52012, 20209},
	{52013, 20209},
	{52014, 20209},
	{52015, 20209},
	
	{52016, 20202},
	{52017, 20202},
	{52018, 20202},
	{52019, 20202},
	
	{52020, 20202},
	{52021, 20206},
	{52022, 20206},
	{52023, 20206},
	
	{52024, 20206},
	{52025, 20206},
	{52026, 20210},
	{52027, 20210},
	
	{52028, 20210},
	{52029, 20210},
	{52030, 20204},
	{52031, 20204},
	
	{52032, 20204},
	{52033, 20204},
	{52034, 20204},
	{52035, 20204},
	
	{52036, 20208},
	{52037, 20208},
	{52038, 20208},
	{52039, 20208},
	
	{52040, 20208},
	{52041, 20212},
	{52042, 20212},
	{52043, 20212},
	
	{52044, 20212},
	{52045, 20212},
	{52046, 20203},
	{52047, 20203},
	
	{52048, 20203},
	{52049, 20203},
	{52050, 20203},
	{52051, 20207},
	
	{52052, 20207},
	{52053, 20207},
	{52054, 20207},
	{52055, 20207},
	
	{52056, 20211},
	{52057, 20211},
	{52058, 20211},
	{52059, 20211},
	
	{52060, 20211},
	{52061, 20213},
	{52062, 20213},
	{52063, 20213},
	
	{52064, 20213},
	{52065, 20213},
	{52066, 20214},
	{52067, 20214},
	
	{52068, 20214},
	{52069, 20214},
	{52070, 20214},
	{52071, 20215},
	
	{52072, 20215},
	{52073, 20215},
	{52074, 20215},
	{52075, 20215},
	
	{71114, 20110},
	{71115, 20110},
	{71116, 20111},
	{71117, 20111},
	
	{71118, 20112},
	{71119, 20112},
	{71120, 20113},
	{71121, 20113},
	
	{71124, 20114},
	{71125, 20115},
	{71126, 20116},
	{71127, 20117},
	
	{71128, 20118},
	{71137, 20120},
	{71138, 20112},
	{71139, 20122},
	
	{71140, 20123},
	{71141, 20124},
	{71142, 20125},
};
extern "C" void CHARACTER__Restart(SCHAR* a1, const char* a2, int a3, int a4)
{
	if (!a1->IsDead())
	{
		a1->ChatPacket(5, "CloseRestartWindow"); //CloseRestartWindow
		return;
	}
	if (!a1->DeadEvent)
	{
		return;
	}
	/* If on Guild's war */
	// jeżeli wojna gildii
	int v19 = *(DWORD*)((DWORD)a1 + 3276);
	if (v19)
	{
		if (!a1->isObserver)
		{
			int v20 = ((int(*)(int,SCHAR*))0x081DF290)(v19, a1);
			if (v20)
			{
				a1->SetPosition(7);
				a1->StartRecoveryEvent();
				a1->ChatPacket(5, "CloseRestartWindow"); //CloseRestartWindow
				if (a4)
				{
					a1->ViewReencode();
				}
				else
				{
					int v21 = a1->guild->guild_id;
					int kords[2];
					char xx = ((char(*)(int,int,BYTE,int*))0x081DF370)(*(DWORD*)(0x84C5C84), a1->map_index, v20 <= v21, kords);
					if (xx)
					{
						a1->Show(a1->map_index, kords[0], kords[1], -1, 0);
					}
					else
					{
						a1->ExitToSavedLocation();
					}
				}
				a1->ChangePoint(5, a1->max_hp - a1->hp, 0, 0);
				a1->ChangePoint(7, a1->max_sp - a1->sp, 0, 0);
				a1->ReviveInvisible(5);

				a1->RefreshCostumeBonuses();
				return;
			}
		}
	}

	//Wszystko reszta jeżeli nie wojna gildii
	/*Everything else if not on war */

	int v6 = event_time(a1->DeadEvent);
	int v8 = v6 / PASSES_PER_SEC;
	if (a4)
	{
		if (a1->map_index >= 2350000 && a1->map_index <= 2359999)
		{
			a1->ChatPacket(1, locale_find("npvpt")); // Nie możesz się odrodzić podczas drużynowego pojedynku -- npvpt
			return;
		}
		if (v8 > 170)
		{
			int v13 = v8 - 170;
			a1->ChatPacket(1, locale_find("ninutpvp %dch"), v13); // Wznowienie nie jest możliwe (pozostało %d sekund) -- ninutpvp %dch
			return ;
		}
		a1->ChatPacket(5, "CloseRestartWindow"); //CloseRestartWindow
		a1->SetPosition(7);
		int hp = 0;

		if (a1->map_index == 255)
		{
			hp = a1->max_hp - a1->hp;
		}
		else
		{
			hp = 50 - a1->hp;
		}
		a1->ChangePoint(5, hp, 0, 0);
		a1->StartRecoveryEvent();

		int xx_k;
		int yy_k;
		if (a1->map_index == 255)
		{
			xx_k = 4000000;
			yy_k = 102400;

			xx_k += RandomNumber(10400, 17200);
			yy_k += RandomNumber(10000, 14900);
		}
		else
		{
			xx_k = a1->x;
			yy_k = a1->y;
		}
		a1->Show(a1->map_index, xx_k, yy_k, -1, 0);
		a1->ReviveInvisible(5);
		if (!a3)
		{
			((void(*)(SCHAR*, char))0x08083EB0)(a1, 0); // CHARACTER::DeathPenetly
		}
		a1->RefreshCostumeBonuses();
		
		SCHAR* p = a1;
		if (!p->poly_vnum)
		{
			ITEM* v1 = p->GetWear(7);
			if (v1 && v1->GetSpecialGroup() == 10030)
			{
				int id = v1->ItemProto->ID;
				for (int i = 0; i < 95; i ++)
				{
					if (id == mount_table[i][0])
					{
						int x = mount_table[i][1];
						p->AddAffect(221,109,x,0,v1->socket[2],0,1,0);
						break;
					}
				}
			}
			else
			{
				ITEM* v2 = p->GetWear(8);
				if (v2 && v2->GetSpecialGroup() == 10030)
				{
					int id = v2->ItemProto->ID;
					for (int i = 0; i < 95; i ++)
					{
						if (id == mount_table[i][0])
						{
							int x = mount_table[i][1];
							p->AddAffect(221,109,x,0,v2->socket[2],0,1,0);
							break;
						}
					}
				}
			}
		}

		a1->ViewReencode();
	}
	else
	{
		if (v8 > 173)
		{
			int v13 = v8 - 173;
			a1->ChatPacket(1, locale_find("nogetup%ds"), v13); // Wznowienie nie jest możliwe (pozostało %d sekund) -- nogetup%ds
			return;
		}
		a1->ChatPacket(5, "CloseRestartWindow"); //CloseRestartWindow
		int arr1[2];
		int arr2[2];

		SECTREE_MANAGER* sm = SECTREE_MANAGER_SINGLETON;
		bool xx = sm->GetRecallPositionByEmpire(a1->map_index, a1->empire, arr2);
		if (xx)
		{
			arr1[0] = arr2[0];
			arr1[1] = arr2[1];
		}
		else
		{
			arr1[0] = 0;
			arr1[1] = 0;

			BYTE e = a1->empire;
			if (e <=3)
			{
				arr1[0] = *(DWORD*)(START_POSTION+e*8);
				arr2[0] = *(DWORD*)(START_POSTION+e*8+4);
			}
		}
		a1->SetWarp(arr1[0], arr1[1], 0);
		a1->ChangePoint(5, 50-a1->hp, 0, 0);
	}
}

extern "C" void CHARACTER__ItemDropPenalty(SCHAR* a1)
{
	if (a1->map_index != 255)
	{
		((void(*)(SCHAR*))0x08084280)(a1); //CHARACTER::ItemDropPenalty
	}
}

extern "C" void CHARACTER__PointChance_AntyExp(SCHAR* a1, BYTE a2, int a3, bool a4, bool a5)
{
	ITEM* v1 = a1->GetWear(7);
	if (v1 && v1->ItemProto->ID == 72731)
	{
		return;
	}
	v1 = a1->GetWear(8);
	if (v1 && v1->ItemProto->ID == 72731)
	{
		return;
	}
	a1->ChangePoint(a2,a3,a4,a5);
}

extern "C" int CHARACTER__IsPartyJoinableCondition(SCHAR* a1, SCHAR* a2)
{
	int result = 10;
	if (a1->map_index != 255)
	{
		if (a1->empire == a2->empire)
		{
			result = ((int(*)(SCHAR*, SCHAR*))0x806ECC0)(a1,a2);
		}
	}
	return result;
}

extern "C" void CHARACTER__SetPKMode(SCHAR* a1,char a2)
{
	if (a1->map_index >= 2350000 && a1->map_index <= 2359999 || a1->map_index == 255)
	{
		return;
	}
	a1->SetPKMode(a2);
}

extern "C" void CHARACTER__SkillLevelUp(SCHAR* a1, int a2, char a3)
{
	if (a2 != 131)
	{
		((void(*)(SCHAR*, int, char))0x80AE500)(a1,a2,a3); // CHARACTER::SkillLevelUp
	}
}

extern "C" void DeadHorseWillDIE(SCHAR* a1, bool a2, bool a3)
{
	/* If you try to summon your horse and you didn't feed him for a long time
	and it's dead, you will summon a horse corpse and for some reason
	that corpse doesn't despawn after you log out. So you can summon it again
	after you reconnect. It's actually pretty funny, but can crash the server
	if you do it for enough long time */
	
	if (a1->horse_life <= 0)
	{
		a1->ChatPacket(1, locale_find("hdhhor ded")); // Twój koń nie żyje -- hdhhor ded
		return;
	}
	((void(*)(SCHAR*,bool,bool))0x80908F0)(a1,a2,a3);
}

extern "C" bool CHARACTER__IsImmune(SCHAR* a1, char a2)
{
	ITEM* item = a1->GetWear(10);
	if (item)
	{
		int id;
		if (a2 == 2)
		{
			id = 14;
		}
		else
		{
			id = 48;
		}
		for (int i = 0; i<5;i++)
		{
			if (item->bonus[i].id == id)
			{
				if (RandomNumber(1,10) <= 9)
				{
					if (TEST_SERVER)
					{
						a1->ChatPacket(3, "ImmuneFlag Activated !"); // ImmuneFlag Activated !
					}
					return 1;
				}
			}
		}
	}
	return 0;
	if (TEST_SERVER)
	{
			a1->ChatPacket(3, "ImmuneFlag Fail"); // ImmuneFlag Fail
	}
}

extern "C" int my_enter_game(int a1, int a2)
{
	SCHAR* p = (SCHAR*)*(int*)(a2+76);
	int ret = ( ( int ( * ) ( int, int ) ) 0x08127FD0 )(a1, a2);
	bool update = false;
	if (p)
	{
		if (p->gm_level)
		{
			if (p->GetQuestFlag("gm.obs"))
			{
				((void(*)(SCHAR*,bool))0x080FAF30)(p,1); //SetObserverMode
			}
		}
		if (!p->poly_vnum)
		{
			ITEM* v1 = p->GetWear(7);
			if (v1 && v1->GetSpecialGroup() == 10030)
			{
				int id = v1->ItemProto->ID;
				for (int i = 0; i < 95; i ++)
				{
					if (id == mount_table[i][0])
					{
						int x = mount_table[i][1];
						p->AddAffect(221,109,x,0,v1->socket[2],0,1,0);
						break;
					}
				}
			}
			else
			{
				ITEM* v2 = p->GetWear(8);
				if (v2 && v2->GetSpecialGroup() == 10030)
				{
					int id = v2->ItemProto->ID;
					for (int i = 0; i < 95; i ++)
					{
						if (id == mount_table[i][0])
						{
							int x = mount_table[i][1];
							p->AddAffect(221,109,x,0,v2->socket[2],0,1,0);
							break;
						}
					}
				}
			}
		}
		if (!p->gm_level && p->map_index == 113)
		{
			((void(*)(SCHAR*))0x08081920)(p); // RemoveGoodAffect
			((void(*)(SCHAR*))0x08081890)(p); // RemoveBadAffect
			for (int i = 0; i <4;i++)
			{
				p->SetPart(i,0);
			}
			p->UpdatePacket();
			*(BYTE*)((DWORD)p + 4) = 1;
			*(BYTE*)((DWORD)p + 5) = 1;
			((void(*)(ENTITY*))0x80FB780)(p); // CEntity::UpdateSectree(p)
		}
		if (p->map_index == 255)
		{
			p->SetPKMode(2);
		}
		std::string QF4 = "kostium.pm"; // kostium.pm
		std::string QF5 = "kostium.ph"; // kostium.ph
		unsigned int time = p->GetQuestFlag("kostium.m_time");

		if (time > 1 && time <= get_global_time())
		{
			p->SetQuestFlag(QF4, 1);
			p->ChatPacket(5, "cos 1|1|0"); // cos 1|1|0
		}
		else
		{
			int id = p->GetQuestFlag(QF4);
			if (id > 1)
			{
				p->ChatPacket(5, "cos %d|1|%d", id, time); //cos %d|1|%d
				p->SetPart(0, id);
				update = true;
			}
		}
		time = p->GetQuestFlag("kostium.h_time"); // kostium.h_time
		if (time > 1 && time <= get_global_time())
		{
			p->SetQuestFlag(QF5, 1);
			p->ChatPacket(5, "cos 1|0|0"); // cos 1|0|0
		}
		else
		{
			int id = p->GetQuestFlag(QF5);
			if (id > 1)
			{
				p->ChatPacket(5, "cos %d|0|%d", id, time); // cos %d|0|%d
				p->SetPart(3, 0);
				update = true;
			}
		}
		if(update)
		{
			p->UpdatePacket();
		}

		/*if (!p->poly_vnum)
		{
			ITEM* v1 = CHARACTER_GetWear(p, 7);
			if (v1 && ITEM_GetSpecialGroup(v1) == 10030)
			{
				int id = v1->ItemProto->ID;
				for (int i = 0; i<25;i++)
				{
					if (id == mount_table[i][0])
					{
						int x = mount_table[i][1];
						CHARACTER_AddAffect(p, 221, 109, x, 0, v1->socket[2], 0, 1, 0);
						return ret;
					}
				}
			}

			v1 = CHARACTER_GetWear(p, 8);
			if (v1 && ITEM_GetSpecialGroup(v1) == 10030)
			{
				int id = v1->ItemProto->ID;
				for (int i = 0; i<25;i++)
				{
					if (id == mount_table[i][0])
					{
						int x = mount_table[i][1];
						CHARACTER_AddAffect(p, 221, 109, x, 0, v1->socket[2], 0, 1, 0);
						return ret;
					}
				}	
			}
		}
		if (!p->gm_level && p->map_index == 113)
		{
			((void(*)(SCHAR*))0x08081920)(p); //RemoveGoodAffects
			((void(*)(SCHAR*))0x08081890)(p);//RemoveBadAffects
			for (int i = 0;i<4;i++)
			{
				CHARACTER_SetPart(p, i, 0);
			}
			CHARACTER_UpdatePacket(p);
			p->isObservator = 1;
			((void(*)(SCHAR*))0x80FB6B0)(p); //CENTITY::UpdateSectree()
		}
		if (p->poly_vnum == 30509)
		{
			CHARACTER_RemoveAffect(p, 220);
		}*/
	}
	
	return ret;
}

extern "C" void Player_Destroy_hook_c(SCHAR* a1)
{
	if (a1 && a1->lpDesc)
	{
		a1->isDestroyed = 1;
		SCHAR* pecik = a1->pet;
		if (pecik)
		{
			pecik->rider = 0;
		}

		//if (a1->lobby)
		//{
		//	leave_lobby(a1);
		//}
		a1->RemoveAffect(1002);
		a1->RemoveAffect(1003);
	}
}

extern "C" void Player_Destroy_hook_asm(void)
{
	asm(".intel_syntax noprefix\n"
		".globl Player_Destroy_hook_c\n"
		"add esp, 4\n"
		"mov [esp], esi\n"
		"mov eax, 0x8078400\n"
		"call eax\n"
		"push esi\n"
		"call Player_Destroy_hook_c\n"
		"add esp, 4\n"
		"push 0x0807852E\n"
		"ret\n"
	);
}

void HOOK__CHARACTER_HOOKS(int offset)
{
	int PartyJ[3] = { 0x0806EEF1, 0x0807C71A, offset+0x0807C8EE};
	for (int i = 0; i <3; i++)
	{
		CallPatch((int)&CHARACTER__IsPartyJoinableCondition, IPS(offset+PartyJ[i]), 0);
	}
	CallPatch((int)&CHARACTER__PointChance_AntyExp, IPS(offset+0x08086D7F), 0);
	CallPatch((int)&CHARACTER__ItemDropPenalty, IPS(offset+0x080891A8), 0);

	CallPatch((int)&CHARACTER__SetPKMode, IPS(offset+0x080C3FF9), 0);

	CallPatch((int)&CHARACTER__SkillLevelUp, IPS(offset+0x080C6656), 0);

	CallPatch((int)&DeadHorseWillDIE, IPS(offset+0x0818E8D7), 0);

	mprotect((const void*)(CMD_INFO+0x374), 4, 7);
	mprotect((const void*)(CMD_INFO+0x388), 4, 7);

	*(DWORD*)(CMD_INFO+0x374) = (DWORD)&CHARACTER__Restart;
	*(DWORD*)(CMD_INFO+0x388) = (DWORD)&CHARACTER__Restart;

	int immune[10] = { 0x08061104, 0x080611B8, 0x080BD476, 
						0x080BD579, 0x080BD67F, 0x080BDE3E, 
						0x080CF661, 0x080CF661, 0x081DDF06, 
						0x081EDACB };
	for (int i = 0;i<10;i++)
	{
		CallPatch((int)&CHARACTER__IsImmune, IPS(offset+immune[i]), 0);
	}

	CallPatch((int)&my_enter_game, IPS(offset+0x08129457), 0);

	CallPatch((int)&Player_Destroy_hook_asm+3, IPS(offset+0x08078529), 0);
}

