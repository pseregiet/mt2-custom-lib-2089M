#ifndef __LIB_DESC_HPP
#define __LIB_DESC_HPP

/*DESC is a Class used by every player
It is mainly a class that manages client's account connection with server
*/
#include "types.h"

class SCHAR;

#pragma pack(1)
struct SSimplePlayer
{
	DWORD dwID;
    char szName[25];
    BYTE byJob;
    BYTE byLevel;
    DWORD dwPlayMinutes;
    BYTE byST;
    BYTE byHT;
    BYTE byDX;
    BYTE byIQ;
    WORD wMainPart;
    BYTE bChangeName;
    WORD wHairPart;
    BYTE bDummy[4];
    long x;
    long y;
    long lAddr;
    WORD wPort;
    BYTE skill_group;
};
#pragma pack()

#pragma pack(1)
struct SAccountTable
{
    DWORD id;
    char login[31];
    char passwd[17];
    char social_id[19];
    char status[9];
    BYTE bEmpire;
    SSimplePlayer players[4];
};
#pragma pack()

#pragma pack(1)
class DESC
{
public:
	DESC(void);
	~DESC(void);

	char unkn[76]; // 0 - 75
	SCHAR* m_lpSCHAR;
	SAccountTable m_accountTable;

	void DelayedDisconnect(int sec);
	void Packet(const void* packet, int size);

};
#pragma pack()

#endif
