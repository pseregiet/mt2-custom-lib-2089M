#ifndef __LIB_COMMANDS_HPP
#define __LIB_COMMANDS_HPP

#include <cstdlib>
#include "types.h"
#include "SCHAR.h"
#include "ITEM.h"
#include "hook_tools.h"
#include "SCHAR_MANAGER.h"
#include "misc.h"
#include "sql.h"
#include "costumes.h"
//#include "pets.h"

#define NOARG_COMMAND 6
#define ARG_COMMAND 4

void HOOK_COMMANDS(int offset);

#endif
