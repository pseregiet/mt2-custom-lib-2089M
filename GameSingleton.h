#ifndef __LIB_GAME_SINGLETON_HPP
#define __LIB_GAME_SINGLETON_HPP
#include <cstddef>
template <typename T, unsigned int TAddr>
class GameSingleton
{
  public:
    static T * ms_singleton;
    GameSingleton(void){};
    ~GameSingleton(){};
    static T *instance()
	{
        if (!ms_singleton)
		{
            ms_singleton=*(T**)TAddr;
        }
        return ms_singleton;
    };
};
template <typename T, unsigned int TAddr> T* GameSingleton<T,TAddr>::ms_singleton = NULL;
#endif

