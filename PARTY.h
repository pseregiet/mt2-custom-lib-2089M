#ifndef __LIB_PARTY_HPP
#define __LIB_PARTY_HPP

#include "types.h"
#include "SCHAR.h"

class DUNGEON;

#pragma once
class PARTY
{
public:
	PARTY(void);
	~PARTY(void);
	void SetDungeon(DUNGEON* dung);
	bool CheckLevel(int level);
	int MemberCount(void);
	int LiveCountOnMap(int map);
	bool CheckQF(std::string qf);
};

#endif


