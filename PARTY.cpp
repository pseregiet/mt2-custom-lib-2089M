#include "PARTY.h"


PARTY::PARTY(void)
{
}


PARTY::~PARTY(void)
{
}


void PARTY::SetDungeon(DUNGEON* dung)
{
	((void(*)(PARTY*, DUNGEON*))0x0816BDD0)(this,dung);
}



bool PARTY::CheckLevel(int level)
{
	bool ret = true;
	for (DWORD i = *(DWORD*)((DWORD)this + 16);(DWORD)this+8 !=i;
		i = ((int(*)(int))0x0804CD4C)(i) )
	{
		SCHAR* a1 = (SCHAR*)*(DWORD*)(i+20);
		if (a1)
		{
			if (a1->level < level)
			{
				ret = false;
				break;
			}
		}
	}
	return ret;
}


int PARTY::MemberCount(void)
{
	int ret = 0;
	for (DWORD i = *(DWORD*)((DWORD)this + 16);(DWORD)this+8 !=i;
		i = ((int(*)(int))0x0804CD4C)(i) )
	{
		SCHAR* a1 = (SCHAR*)*(DWORD*)(i+20);
		if (a1)
		{
			if (a1->lpDesc)
			{
				ret++;
			}
		}
	}
	return ret;
}



int PARTY::LiveCountOnMap(int map)
{
	int ret = 0;
	for (DWORD i = *(DWORD*)((DWORD)this + 16);(DWORD)this+8 !=i;
		i = ((int(*)(int))0x0804CD4C)(i) )
	{
		SCHAR* a1 = (SCHAR*)*(DWORD*)(i+20);
		if (a1 && a1->lpDesc && a1->map_index == map)
		{
			ret ++;
		}
	}
	return ret;
}

bool PARTY::CheckQF(std::string qf)
{
	for (DWORD i = *(DWORD*)((DWORD)this + 16);(DWORD)this+8 !=i;
		i = ((int(*)(int))0x0804CD4C)(i) )
	{
		SCHAR* a1 = (SCHAR*)*(DWORD*)(i+20);
		if (a1 && a1->lpDesc)
		{
			if ( ((DWORD)a1->GetQuestFlag(qf) < get_global_time()))
			{
				return true;
			}
		}
	}
	return false;
}
