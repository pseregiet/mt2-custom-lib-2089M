#include "SCHAR_MANAGER.h"

SCHAR* SCHAR_MANAGER::FindPC(const char* a1)
{
	return ((SCHAR*(*)(SCHAR_MANAGER*, const char*))0x080A6530)(this, a1);
}

SCHAR* SCHAR_MANAGER::Find(int vid)
{
	return ((SCHAR*(*)(SCHAR_MANAGER*,int))0x080A4F50)(this, vid);
}

void SCHAR_MANAGER::DestroyCharacter(SCHAR* a1)
{
	((void(*)(SCHAR_MANAGER*,SCHAR*))0x080A6660)(this,a1);
}

SCHAR* SCHAR_MANAGER::SpawnMob(DWORD vnum, long map_index, long x, long y, long a5, bool a6, int a7, bool a8)
{
	return ((SCHAR*(*)(SCHAR_MANAGER*,DWORD,long,long,long,long,bool,int,bool))0x080A7040)(this,vnum,map_index,x,y,a5,a6,a7,a8);
}
