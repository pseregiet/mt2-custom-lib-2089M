#ifndef __TENMA_DB_MANAGER_HPP
#define __TENMA_DB_MANAGER_HPP

#include "GameSingleton.h"

#define DB_MANAGER_SINGLETON (DB_MANAGER*)*(DWORD*)(0x84C5B78)

#pragma once
class DB_MANAGER : public GameSingleton <DB_MANAGER, 0x84C5B78>
{
public:
	void Query(const char* q);
	int DirectQuery(const char* q);
};


#endif
