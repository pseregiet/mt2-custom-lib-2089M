#include "ITEM_MANAGER.h"

item_proto* ITEM_MANAGER::GetTable(int id)
{
	return ((item_proto*(*)(ITEM_MANAGER*,int))0x0813CDE0)(this, id);
}
void ITEM_MANAGER::RemoveItem(ITEM* a1, char* name_log)
{
	((void(*)(ITEM_MANAGER*, ITEM*, char*))0x0813E390)(this, a1, name_log);
}
void ITEM_MANAGER::DestroyItem(ITEM* a1)
{
	((void(*)(ITEM_MANAGER*,ITEM*))0x813DFC0)(this, a1);
}
ITEM* ITEM_MANAGER::CreateItem(int a1, int a2, int a3, char a4, int a5)
{
	return ((ITEM*(*)(ITEM_MANAGER*, int, int, int, char, int))0x0813E4F0)(this,a1,a2,a3,a4,a5);
}
