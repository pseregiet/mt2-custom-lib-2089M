#ifndef __LIB_SECTREE_MANAGER_HPP
#define __LIB_SECTREE_MANAGER_HPP

#include "SECTREE.h"
#include "SECTREE_MAP.h"
#include "types.h"

#include "GameSingleton.h"

#define SECTREE_MANAGER_SINGLETON (SECTREE_MANAGER*)*(DWORD*)(0x84C4864)

#pragma once
class SECTREE_MANAGER
{
public:

	SECTREE_MAP* GetMap(long a1);
	SECTREE_MAP* Get(DWORD, DWORD, DWORD);
	bool GetRecallPositionByEmpire(int a1,BYTE a2, int* a3);
	int GetMonsterCountInMap(long map, DWORD vnum);
};

#endif



