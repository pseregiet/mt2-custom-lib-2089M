#include "hook_tools.h"

extern "C" void CallPatch(int pDest, int pSrc, int nNops)
{
	mprotect((const void*)pSrc, 5 + nNops, 7);

	*(unsigned char*)(pSrc) = 0xE8;
	*(unsigned int*)(pSrc + 1) = pDest - pSrc - 5;
	int i;
	for (i = 0; i < nNops; ++i)
	{ 
		*(unsigned char*)(pSrc + 5 + i) = 0x90; 
	}

}

extern "C" void JmpPatch(int pDest, int pSrc, int nNops)
{
	mprotect((const void*)pSrc, 5 + nNops, 7);

	*(unsigned char*)(pSrc) = 0xE9;
	*(unsigned int*)(pSrc + 1) = pDest - pSrc - 5;
	int i;
	for (i = 0; i < nNops; ++i)
	{ 
		*(unsigned char*)(pSrc + 5 + i) = 0x90; 
	}

}

extern "C" void patch(int x, unsigned char* y, int z)
{
	mprotect((const void*)x, z, 7);
	memcpy((void*)x, y, z);
}

extern "C" void fill_nop(int x, int z)
{
	mprotect((const void*)x, z, 7);
	for (int i = 0; i < z;i++)
	{
		*(unsigned char*)(x+i) = 0x90;
	}
}
