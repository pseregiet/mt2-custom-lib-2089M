#ifndef __LIB_TYPES_HPP
#define __LIB_TYPES_HPP

#define DWORD unsigned int
#define WORD unsigned short
#define BYTE unsigned char
#define Event unsigned int

#define revision 2089

#endif
