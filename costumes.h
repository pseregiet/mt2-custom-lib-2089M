#ifndef __LIB_COSTUMES_HPP
#define __LIB_COSTUMES_HPP

#include "hook_tools.h"
#include "misc.h"
#include "types.h"
#include "SCHAR.h"
#include "ITEM.h"
#include "ITEM_MANAGER.h"

extern "C" bool CreateCostumeItem(SCHAR* a1, char a2, int a3, char x);
extern "C" bool unequip_costume(SCHAR* a1, char a2, char x);
extern "C" void equip_costume(SCHAR* a1, ITEM* a2, char a3);


void COSTUMES_HOOK(int offset);

#endif
