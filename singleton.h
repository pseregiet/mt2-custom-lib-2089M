#ifndef __LIB_SINGLETON_HPP
#define __LIB_SINGLETON_HPP
#include <cstddef>
template <typename T>
class Singleton
{
  public:
    static T * ms_singleton;
    static T *instance()
	{
        if (!ms_singleton)
		{
            ms_singleton= new T();
        }
        return ms_singleton;
    };
};
template <typename T> T* Singleton<T>::ms_singleton = NULL;
#endif

