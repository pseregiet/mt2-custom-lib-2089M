#ifndef __LIB_ENTITY_HPP
#define __LIB_ENTITY_HPP

/*Entity is a class that I think I don't have to explain
AS far as i know there's no entity object on itself nowhere in the original code
It's always a child of other class */
class DESC;
class SECTREE;

#pragma pack(1)
class ENTITY
{
public:
	char unkn1[4];
	bool isObserver; /* If an Entity is an Observer, then server doesn't send it's position to the client. It's "invisible" */
	char unkn2[27];
	int map_index;
	DESC* lpDesc;
	char unkn3[8];
	int x;
	int y;
	int z;
	int m_iViewAge;
	SECTREE* pSectree;

	void PacketAround(const void* a1, int a2, ENTITY* a3);
	bool IsType(int a1);
	void ViewReencode(void);

};
#pragma pack()

#endif

