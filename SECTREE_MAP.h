#ifndef __LIB_SECTREE_MAP_HPP
#define __LIB_SECTREE_MAP_HPP

#include "SECTREE.h"
#include "types.h"

class SECTREE;

#pragma once
class SECTREE_MAP
{
public:

	SECTREE* Find(DWORD a1, DWORD a2);
};

#endif


