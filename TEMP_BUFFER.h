#ifndef __LIB_TEMP_BUFFER_HPP
#define __LIB_TEMP_BUFFER_HPP

#pragma once
class TEMP_BUFFER
{
public:
	TEMP_BUFFER(int, bool);
	~TEMP_BUFFER();
	const void * read_peek(void);
	void write(const void *, int);
	int size(void);
	void reset(void);
};

#endif


